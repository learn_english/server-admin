<?php

use Illuminate\Http\Request;
use Illuminate\Contracts\Routing\Registrar;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Kiểm tra đăng nhập
Route::group(['prefix' => 'auth'], function () {
    Route::post('register','AuthController@register');
    Route::post('login', 'AuthController@login');
});

// check logout
Route::group([
    'prefix' => 'auth',
], function (Registrar $router) {
    $router->group(['middleware' => ['auth:api']], function (Registrar $router) {
        $router->get('logout', 'AuthController@logout');
        $router->get('me', 'AuthController@me');
    });
});

// Tài khoản người sử dụng
Route::group([
    'prefix' => 'user',
], function (Registrar $router) {
    $router->group(['middleware' => ['auth:api']], function (Registrar $router) {
        $router->get('list', 'UserController@index');
        $router->get('getPaginate', 'UserController@getPaginate');
        $router->get('isActive', 'UserController@isActive');
        $router->post('create', 'UserController@create');
        $router->get('show/{id}', 'UserController@show');
        $router->post('update/{id}', 'UserController@update');
        $router->delete('remove/{id}', 'UserController@remove');
        $router->post('searchAll', 'UserController@searchAll');

        // Tình trạng tài khoản
        $router->post('checkActive/{id}/{active}', 'UserController@checkActive');
    });
});

// Danh mục
Route::group([
    'prefix' => 'category',
], function (Registrar $router) {
    $router->group(['middleware' => ['auth:api']], function (Registrar $router) {
        $router->get('list', 'CategoryController@index');
        $router->get('getPaginate', 'CategoryController@getPaginate');
        $router->post('create', 'CategoryController@create');
        $router->get('show/{id}', 'CategoryController@show');
        $router->post('update/{id}', 'CategoryController@update');
        $router->delete('remove/{id}', 'CategoryController@remove');
        $router->post('searchAll', 'CategoryController@searchAll');
        $router->post('checkActive/{id}/{active}', 'CategoryController@checkActive');
    });
});

// Nhận bản tin
Route::group([
    'prefix' => 'newsletter',
], function (Registrar $router) {
    $router->group(['middleware' => ['auth:api']], function (Registrar $router) {
        $router->get('list', 'NewsletterController@index');
        $router->get('getPaginate', 'NewsletterController@getPaginate');
        $router->get('show/{id}', 'NewsletterController@show');
        $router->delete('remove/{id}', 'NewsletterController@remove');
        $router->post('searchAll', 'NewsletterController@searchAll');
    });
});

// Đăng kí khóa học
Route::group([
    'prefix' => 'registerCourse',
], function (Registrar $router) {
    $router->group(['middleware' => ['auth:api']], function (Registrar $router) {
        $router->get('list', 'RegisterCourseController@index');
        $router->get('getPaginate', 'RegisterCourseController@getPaginate');
        $router->get('show/{id}', 'RegisterCourseController@show');
        $router->delete('remove/{id}', 'RegisterCourseController@remove');
        $router->post('searchAll', 'RegisterCourseController@searchAll');
    });
});

// Khóa học trực tiếp
Route::group([
    'prefix' => 'liveCourse',
], function (Registrar $router) {
    $router->group(['middleware' => ['auth:api']], function (Registrar $router) {
        $router->get('list', 'LiveCourseController@index');
        $router->get('getPaginate', 'LiveCourseController@getPaginate');
        $router->post('create', 'LiveCourseController@create');
        $router->get('show/{id}', 'LiveCourseController@show');
        $router->post('update/{id}', 'LiveCourseController@update');
        $router->delete('remove/{id}', 'LiveCourseController@remove');
        $router->post('searchAll', 'LiveCourseController@searchAll');
        $router->post('upload', 'LiveCourseController@upload');
        $router->post('checkActive/{id}/{active}', 'LiveCourseController@checkActive');
    });
});

// Khóa học online
Route::group([
    'prefix' => 'onlineCourse',
], function (Registrar $router) {
    $router->group(['middleware' => ['auth:api']], function (Registrar $router) {
        $router->get('list', 'OnlineCourseController@index');
        $router->get('getPaginate', 'OnlineCourseController@getPaginate');
        $router->post('create', 'OnlineCourseController@create');
        $router->get('show/{id}', 'OnlineCourseController@show');
        $router->post('update/{id}', 'OnlineCourseController@update');
        $router->delete('remove/{id}', 'OnlineCourseController@remove');
        $router->post('searchAll', 'OnlineCourseController@searchAll');
        $router->post('upload', 'OnlineCourseController@upload');
        $router->post('checkActive/{id}/{active}', 'OnlineCourseController@checkActive');
    });
});

// Khóa học qua APP
Route::group([
    'prefix' => 'appCourse',
], function (Registrar $router) {
    $router->group(['middleware' => ['auth:api']], function (Registrar $router) {
        $router->get('list', 'AppCourseController@index');
        $router->get('getPaginate', 'AppCourseController@getPaginate');
        $router->post('create', 'AppCourseController@create');
        $router->get('show/{id}', 'AppCourseController@show');
        $router->post('update/{id}', 'AppCourseController@update');
        $router->delete('remove/{id}', 'AppCourseController@remove');
        $router->post('searchAll', 'AppCourseController@searchAll');
        $router->post('upload', 'AppCourseController@upload');
        $router->post('checkActive/{id}/{active}', 'AppCourseController@checkActive');
    });
});

// Bài đăng
Route::group([
    'prefix' => 'post',
], function (Registrar $router) {
    $router->group(['middleware' => ['auth:api']], function (Registrar $router) {
        $router->get('list', 'PostController@index');
        $router->get('getPaginate', 'PostController@getPaginate');
        $router->post('create', 'PostController@create');
        $router->get('show/{id}', 'PostController@show');
        $router->post('update/{id}', 'PostController@update');
        $router->delete('remove/{id}', 'PostController@remove');
        $router->post('upload', 'PostController@upload');
        $router->post('searchAll', 'PostController@searchAll');
        $router->post('checkActive/{id}/{active}', 'PostController@checkActive');
    });
});

// Bài đăng con (thuộc bài đăng cha)
Route::group([
    'prefix' => 'subPost',
], function (Registrar $router) {
    $router->group(['middleware' => ['auth:api']], function (Registrar $router) {
        $router->get('list', 'SubPostController@index');
        $router->get('getPaginate', 'SubPostController@getPaginate');
        $router->post('create', 'SubPostController@create');
        $router->get('show/{id}', 'SubPostController@show');
        $router->post('update/{id}', 'SubPostController@update');
        $router->delete('remove/{id}', 'SubPostController@remove');
        $router->post('upload', 'SubPostController@upload');
        $router->post('searchAll', 'SubPostController@searchAll');
        $router->post('checkActive/{id}/{active}', 'SubPostController@checkActive');
    });
});

// Trung tâm giảng dạy
Route::group([
    'prefix' => 'teachingCenter',
], function (Registrar $router) {
    $router->group(['middleware' => ['auth:api']], function (Registrar $router) {
        $router->get('list', 'TeachingCenterController@index');
        $router->get('getPaginate', 'TeachingCenterController@getPaginate');
        $router->post('create', 'TeachingCenterController@create');
        $router->get('show/{id}', 'TeachingCenterController@show');
        $router->post('update/{id}', 'TeachingCenterController@update');
        $router->delete('remove/{id}', 'TeachingCenterController@remove');
        $router->post('upload', 'TeachingCenterController@upload');
        $router->post('searchAll', 'TeachingCenterController@searchAll');
        $router->post('checkActive/{id}/{active}', 'TeachingCenterController@checkActive');
    });
});

// Thông báo
Route::group([
    'prefix' => 'notification',
], function (Registrar $router) {
    $router->group(['middleware' => ['auth:api']], function (Registrar $router) {
        $router->get('list', 'NotificationController@index');
        $router->get('getPaginate', 'NotificationController@getPaginate');
        $router->post('create', 'NotificationController@create');
        $router->get('show/{id}', 'NotificationController@show');
        $router->post('update/{id}', 'NotificationController@update');
        $router->post('searchAll', 'NotificationController@searchAll');
        $router->delete('remove/{id}', 'NotificationController@remove');
        $router->post('checkActive/{id}/{active}', 'NotificationController@checkActive');
    });
});

// Slide
Route::group([
    'prefix' => 'slide',
], function (Registrar $router) {
    $router->group(['middleware' => ['auth:api']], function (Registrar $router) {
        $router->get('list', 'SlideController@index');
        $router->get('getPaginate', 'SlideController@getPaginate');
        $router->post('create', 'SlideController@create');
        $router->get('show/{id}', 'SlideController@show');
        $router->post('update/{id}', 'SlideController@update');
        $router->delete('remove/{id}', 'SlideController@remove');
        $router->post('searchAll', 'SlideController@searchAll');
        $router->post('upload', 'SlideController@upload');
        $router->post('checkActive/{id}/{active}', 'SlideController@checkActive');
    });
});

// Chính sách bảo mật
Route::group([
    'prefix' => 'securityRegulation',
], function (Registrar $router) {
    $router->group(['middleware' => ['auth:api']], function (Registrar $router) {
        $router->get('list', 'SecurityRegulationController@index');
        $router->get('getPaginate', 'SecurityRegulationController@getPaginate');
        $router->post('create', 'SecurityRegulationController@create');
        $router->get('show/{id}', 'SecurityRegulationController@show');
        $router->post('update/{id}', 'SecurityRegulationController@update');
        $router->post('searchAll', 'SecurityRegulationController@searchAll');
        $router->delete('remove/{id}', 'SecurityRegulationController@remove');
        $router->post('checkActive/{id}/{active}', 'SecurityRegulationController@checkActive');
    });
});

// Banner quảng cáo
Route::group([
    'prefix' => 'banner',
], function (Registrar $router) {
    $router->group(['middleware' => ['auth:api']], function (Registrar $router) {
        $router->get('list', 'BannerController@index');
        $router->get('getPaginate', 'BannerController@getPaginate');
        $router->post('create', 'BannerController@create');
        $router->get('show/{id}', 'BannerController@show');
        $router->post('update/{id}', 'BannerController@update');
        $router->delete('remove/{id}', 'BannerController@remove');
        $router->post('searchAll', 'BannerController@searchAll');
        $router->post('upload', 'BannerController@upload');
        $router->post('checkActive/{id}/{active}', 'BannerController@checkActive');
    });
});

// OPTIONS
// Options add post
Route::get('category/getPosts/{id}', 'PostController@getPosts');
Route::get('post/bundle/{id}', 'PostController@bundle');

// TEST
//Route::get('category/getPosts/{id}', 'PostController@getPosts');
// END TEST

