<?php

namespace App;

use App\Support\DataTablePaginate;
use Illuminate\Database\Eloquent\Model;

class RegisterCourse extends Model
{
    use DataTablePaginate;

    protected $fillable = [
        'last_name',
        'mid_name',
        'first_name',
        'email',
        'mobile',
        'address',
        'sex',
        'teaching_center_id',
        'live_course_id',
        'last_name_child',
        'mid_name_child',
        'first_name_child',
        'birthday_child',
        'why_know',
    ];

    protected $filter = [
        'id',
        'last_name',
        'mid_name',
        'first_name',
        'email',
        'mobile',
        'address',
        'sex',
        'teaching_center_id',
        'live_course_id',
        'last_name_child',
        'mid_name_child',
        'first_name_child',
        'birthday_child',
        'why_know',
    ];
}
