<?php

namespace App;

use App\Support\DataTablePaginate;
use Illuminate\Database\Eloquent\Model;

class Slide extends Model
{
    use DataTablePaginate;

    protected $fillable = [
        'slug',
        'title',
        'short_description',
        'thumbnails',
        'link',
        'status',
    ];

    protected $filter = [
        'id',
        'slug',
        'title',
        'short_description',
        'thumbnails',
        'link',
        'status',
    ];
}
