<?php

namespace App;

use App\Support\DataTablePaginate;
use Illuminate\Database\Eloquent\Model;

class AppCourse extends Model
{
    use DataTablePaginate;

    protected $fillable = [
        'slug',
        'title',
        'price',
        'keywords',
        'short_description',
        'description',
        'thumbnails',
        'type_app',

        'pending_delete',
        'status',
    ];

    protected $filter = [
        'id',

        // BasicInfo
        'slug',
        'title',
        'price',
        'type_app',
        // Description
        'keywords',
        'short_description',
        'description',

        // MediaInfo
        'thumbnails',

        // OrderInfo
        'pending_delete',
        'status',
    ];
}
