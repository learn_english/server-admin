<?php

namespace App;

use App\Support\DataTablePaginate;
use Illuminate\Database\Eloquent\Model;

class LiveCourse extends Model
{
    use DataTablePaginate;

    protected $fillable = [
        'slug',
        'title',
//        'teaching_center_id',
        'price',
        'address',
        'keywords',
        'short_description',
        'description',
        'youtube',
//        'author',
//        'approved_by',
//        'approved_at',
//        'rejected_by',
//        'rejected_at',
        'pending_delete',
        'status',
    ];

    protected $filter = [
        'id',

        // BasicInfo
        'slug',
        'title',
//        'teaching_center_id',
        'price',
        'address',
        // Description
        'keywords',
        'short_description',
        'description',

        // MediaInfo
        'youtube',

        // CheckInfo
//        'author',
//        'approved_by',
//        'approved_at',
//        'rejected_by',
//        'rejected_at',

        // OrderInfo
        'pending_delete',
        'status',
    ];
}
