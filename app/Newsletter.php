<?php

namespace App;

use App\Support\DataTablePaginate;
use Illuminate\Database\Eloquent\Model;

class Newsletter extends Model
{
    use DataTablePaginate;

    protected $fillable = [
        'full_name',
        'email',
        'mobile',
        'address',
        'sex'
    ];

    protected $filter = [
        'id',
        'full_name',
        'email',
        'mobile',
        'address',
        'sex'
    ];
}
