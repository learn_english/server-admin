<?php

namespace App;

use App\Support\DataTablePaginate;
use Illuminate\Database\Eloquent\Model;

class PostLiveCourse extends Model
{
    use DataTablePaginate;

    protected $table = "post_live_courses";

    protected $fillable = [
        'post_id',
        'live_course_id',
    ];

    protected $filter = [
        'id',
        'post_id',
        'live_course_id',
    ];
}
