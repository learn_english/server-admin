<?php

namespace App;

use App\Support\DataTablePaginate;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    use DataTablePaginate;

    protected $fillable = [
        'slug',
        'title',
        'keywords',
        'short_description',
        'description',
        'thumbnails',
//        'author',
//        'approved_by',
//        'approved_at',
//        'rejected_by',
//        'rejected_at',
        'pending_delete',
        'status',
        'category_id',
    ];

    protected $filter = [
        'id',
        'slug',
        'title',
        'keywords',
        'short_description',
        'description',
        'thumbnails',
//        'author',
//        'approved_by',
//        'approved_at',
//        'rejected_by',
//        'rejected_at',
        'pending_delete',
        'status',
        'category_id',
    ];

//    public function schools()
//    {
//        return $this->belongsTo(school::class, 'school_id','school_id');
//    }
//
//    // Quan hệ 1 nhiều (1 đơn vị có nhiều cán bộ)
//    public function staffs()
//    {
//        return $this->hasMany(staff::class, 'unit_id', 'unit_id');
//    }
}
