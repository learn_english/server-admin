<?php

namespace App;

use App\Support\DataTablePaginate;
use Illuminate\Database\Eloquent\Model;

class SecurityRegulation extends Model
{
    use DataTablePaginate;

    protected $fillable = [
        'slug',
        'title',
        'description',
        'status'
    ];

    protected $filter = [
        'id',
        'slug',
        'title',
        'description',
        'status'
    ];
}
