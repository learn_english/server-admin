<?php

namespace App;

use App\Support\DataTablePaginate;
use Illuminate\Database\Eloquent\Model;

class PostOnlineCourse extends Model
{
    use DataTablePaginate;

    protected $table = "post_online_courses";

    protected $fillable = [
        'post_id',
        'online_course_id',
    ];

    protected $filter = [
        'id',
        'post_id',
        'online_course_id',
    ];
}
