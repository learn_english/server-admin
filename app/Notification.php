<?php

namespace App;

use App\Support\DataTablePaginate;
use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    use DataTablePaginate;

    protected $fillable = [
        'slug',
        'title',
        'status',
    ];

    protected $filter = [
        'id',
        'slug',
        'title',
        'status',
    ];
}
