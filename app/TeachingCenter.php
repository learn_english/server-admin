<?php

namespace App;

use App\Support\DataTablePaginate;
use Illuminate\Database\Eloquent\Model;

class TeachingCenter extends Model
{
    use DataTablePaginate;

    protected $fillable = [
        'slug',
        'name',
        'keywords',
        'short_description',
        'info',
        'time_table',
        'contact_table',
        'equipment',
        'longitude',
        'latitude',
        'thumbnails',
        'contact_mobile',
        'contact_phone',
        'contact_email',
        'contact_address',
        'pending_delete',
        'status'
    ];

    protected $filter = [
        'id',
        'slug',
        'name',
        'keywords',
        'short_description',
        'info',
        'time_table',
        'contact_table',
        'equipment',
        'longitude',
        'latitude',
        'thumbnails',
        'contact_mobile',
        'contact_phone',
        'contact_email',
        'contact_address',
        'pending_delete',
        'status'
    ];
}
