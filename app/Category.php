<?php

namespace App;

use App\Support\DataTablePaginate;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use DataTablePaginate;

    protected $fillable = [
        'slug',
        'name',
        'type_menu',
        'ordering',
        'status'
    ];

    protected $filter = [
        'id',
        'slug',
        'name',
        'type_menu',
        'ordering',
        'status'
    ];
}
