<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class OnlineCourseCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'slug'                                                  => 'required',
            'title'                                                 => 'required',
            'price'                                                 => 'required',
            'keywords'                                              => 'required',
            'short_description'                                     => 'required',
            'link'                                                  => 'required',
            'type_online'                                           => 'required',
            'status'                                                => 'required',
        ];
    }

    public function messages()
    {
        return [
            'slug.required'                                         => 'Bạn chưa nhập đường dẫn SEO',
            'title.required'                                        => 'Bạn chưa nhập tiêu đề',
            'price.required'                                        => 'Bạn chưa nhập giá tiền',
            'keywords.required'                                     => 'Bạn chưa nhập từ khóa',
            'short_description.required'                            => 'Bạn chưa nhập mô tả ngắn',
            'link.required'                                         => 'Bạn chưa nhập đường dẫn',
            'type_online.required'                                  => 'Bạn chưa nhập loại online',
            'status.required'                                       => 'Bạn chưa nhập trạng thái',
        ];
    }
}
