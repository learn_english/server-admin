<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BannerCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'slug'                                                  => 'required',
            'title'                                                 => 'required',
            'position'                                              => 'required',
            'keywords'                                              => 'required',
            'short_description'                                     => 'required',
            'thumbnails'                                            => 'required',
            'link'                                                  => 'required',
            'status'                                                => 'required',
        ];
    }

    public function messages()
    {
        return [
            'slug.required'                                         => 'Bạn chưa nhập đường dẫn SEO',
            'title.required'                                        => 'Bạn chưa nhập tiêu đề',
            'position.required'                                     => 'Bạn chưa nhập vị trí',
            'keywords.required'                                     => 'Bạn chưa nhập từ khóa',
            'short_description.required'                            => 'Bạn chưa nhập mô tả ngắn',
            'thumbnails.required'                                   => 'Bạn chưa nhập hình ảnh',
            'link.required'                                         => 'Bạn chưa nhập đường dẫn',
            'status.required'                                       => 'Bạn chưa nhập trạng thái',
        ];
    }
}
