<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class LiveCourseCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'post_id'                                               => 'nullable',
            'slug'                                                  => 'required',
            'title'                                                 => 'required',
            'price'                                                 => 'required',
            'address'                                               => 'required',
            'description'                                           => 'required',
            'keywords'                                              => 'required',
            'short_description'                                     => 'required',
            'youtube'                                               => 'required',
            'status'                                                => 'required',
        ];
    }

    public function messages()
    {
        return [
            'slug.required'                                         => 'Bạn chưa nhập đường dẫn SEO',
            'title.required'                                        => 'Bạn chưa nhập tiêu đề',
            'price.required'                                        => 'Bạn chưa nhập giá tiền',
            'address.required'                                      => 'Bạn chưa nhập địa chỉ',
            'description.required'                                  => 'Bạn chưa nhập mô tả',
            'keywords.required'                                     => 'Bạn chưa nhập từ khóa',
            'short_description.required'                            => 'Bạn chưa nhập mô tả ngắn',
            'youtube.required'                                      => 'Bạn chưa nhập youtube',
            'status.required'                                       => 'Bạn chưa nhập trạng thái',
        ];
    }
}
