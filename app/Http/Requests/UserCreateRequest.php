<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'username'                                                  => 'required',
            'password'                                                  => 'required',
            'address'                                                   => 'required',
            'email'                                                     => 'required',
            'mobile'                                                    => 'required',
            'email_verified_at'                                         => 'required',
            'identity_number'                                           => 'required',
            'last_name'                                                 => 'required',
            'first_name'                                                => 'required',
            'sex'                                                       => 'required',
            'role'                                                      => 'required',
            'status'                                                    => 'required',
        ];
    }

    public function messages()
    {
        return [
            'username.required'                                         => 'Bạn chưa nhập tài khoản',
            'password.required'                                         => 'Bạn chưa nhập mật khẩu',
            'address.required'                                          => 'Bạn chưa nhập địa chỉ',
            'email.required'                                            => 'Bạn chưa nhập email',
            'email_verified_at.required'                                => 'Bạn chưa nhập ngày xác thực email',
            'mobile.required'                                           => 'Bạn chưa nhập số điện thoại',
            'identity_number.required'                                  => 'Bạn chưa nhập cmnd/căn cước',
            'last_name.required'                                        => 'Bạn chưa nhập họ',
            'first_name.required'                                       => 'Bạn chưa nhập tên',
            'sex.required'                                              => 'Bạn chưa nhập giới tính',
            'role.required'                                             => 'Bạn chưa nhập quyền truy cập',
            'status.required'                                           => 'Bạn chưa nhập trạng thái',
        ];
    }
}
