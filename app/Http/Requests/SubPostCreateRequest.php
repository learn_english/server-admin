<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SubPostCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'slug'                                                  => 'required',
            'title'                                                 => 'required',
            'keywords'                                              => 'required',
            'short_description'                                     => 'required',
            'description'                                           => 'required',
            'thumbnails'                                            => 'required',
            'status'                                                => 'required',
            'category_id'                                           => 'required',
            'post_id'                                               => 'required',
        ];
    }

    public function messages()
    {
        return [
            'slug.required'                                         => 'Bạn chưa nhập đường dẫn SEO',
            'title.required'                                        => 'Bạn chưa nhập tiêu đề',
            'keywords.required'                                     => 'Bạn chưa nhập từ khóa',
            'short_description.required'                            => 'Bạn chưa nhập mô tả ngắn',
            'description.required'                                  => 'Bạn chưa nhập mô tả',
            'thumbnails.required'                                   => 'Bạn chưa nhập hình ảnh',
            'status.required'                                       => 'Bạn chưa nhập trạng thái',
            'category_id.required'                                  => 'Bạn chưa nhập danh mục',
            'post_id.required'                                      => 'Bạn chưa nhập bài đăng',
        ];
    }
}
