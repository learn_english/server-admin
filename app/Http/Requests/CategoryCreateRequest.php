<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CategoryCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'slug'                                              => 'required',
            'name'                                              => 'required',
            'type_menu'                                         => 'required',
            'ordering'                                          => 'required',
            'status'                                            => 'required',
        ];
    }

    public function messages()
    {
        return [
            'slug.required'                                     => 'Bạn chưa nhập đường dẫn SEO',
            'name.required'                                     => 'Bạn chưa nhập danh mục',
            'type_menu.required'                                => 'Bạn chưa nhập loại danh mục',
            'ordering.required'                                 => 'Bạn chưa nhập thứ tự ưu tiên',
            'status.required'                                   => 'Bạn chưa nhập trạng thái',
        ];
    }
}
