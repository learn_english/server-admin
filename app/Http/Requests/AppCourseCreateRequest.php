<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AppCourseCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'slug'                                                  => 'required',
            'title'                                                 => 'required',
            'price'                                                 => 'required',
            'description'                                           => 'required',
            'keywords'                                              => 'required',
            'short_description'                                     => 'required',
            'thumbnails'                                            => 'required',
            'type_app'                                              => 'required',
            'status'                                                => 'required',
        ];
    }

    public function messages()
    {
        return [
            'slug.required'                                         => 'Bạn chưa nhập đường dẫn SEO',
            'title.required'                                        => 'Bạn chưa nhập tiêu đề',
            'price.required'                                        => 'Bạn chưa nhập giá tiền',
            'description.required'                                  => 'Bạn chưa nhập mô tả',
            'keywords.required'                                     => 'Bạn chưa nhập từ khóa',
            'short_description.required'                            => 'Bạn chưa nhập mô tả ngắn',
            'thumbnails.required'                                   => 'Bạn chưa nhập hình ảnh',
            'type_app.required'                                     => 'Bạn chưa nhập loại ứng dụng',
            'status.required'                                       => 'Bạn chưa nhập trạng thái',
        ];
    }
}
