<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TeachingCenterCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'slug'                                                  => 'required',
            'name'                                                  => 'required',
            'keywords'                                              => 'required',
            'short_description'                                     => 'required',
            'info'                                                  => 'required',
            'time_table'                                            => 'required',
            'contact_table'                                         => 'required',
            'equipment'                                             => 'required',
            'longitude'                                             => 'required',
            'latitude'                                              => 'required',
            'thumbnails'                                            => 'required',
            'contact_mobile'                                        => 'required',
            'contact_phone'                                         => 'required',
            'contact_email'                                         => 'required',
            'contact_address'                                       => 'required',
            'status'                                                => 'required',
        ];
    }

    public function messages()
    {
        return [
            'slug.required'                                         => 'Bạn chưa nhập đường dẫn SEO',
            'name.required'                                         => 'Bạn chưa nhập tên trung tâm',
            'keywords.required'                                     => 'Bạn chưa nhập từ khóa',
            'short_description.required'                            => 'Bạn chưa nhập mô tả ngắn',
            'info.required'                                         => 'Bạn chưa nhập thông tin',
            'time_table.required'                                   => 'Bạn chưa nhập lịch học',
            'contact_table.required'                                => 'Bạn chưa nhập thông tin liên hệ',
            'equipment.required'                                    => 'Bạn chưa nhập trang thiết bị',
            'longitude.required'                                    => 'Bạn chưa nhập kinh độ',
            'latitude.required'                                     => 'Bạn chưa nhập vĩ độ',
            'thumbnails.required'                                   => 'Bạn chưa nhập hình ảnh',
            'contact_mobile.required'                               => 'Bạn chưa nhập SĐT di động liên hệ',
            'contact_phone.required'                                => 'Bạn chưa nhập SĐT cố định liên hệ',
            'contact_email.required'                                => 'Bạn chưa nhập email liên hệ',
            'contact_address.required'                              => 'Bạn chưa nhập địa chỉ liên hệ',
            'status.required'                                       => 'Bạn chưa nhập trạng thái',
        ];
    }
}
