<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PostCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'slug'                                                  => 'required',
            'title'                                                 => 'required',
            'keywords'                                              => 'required',
            'short_description'                                     => 'required',
            'description'                                           => 'required',
            'thumbnails'                                            => 'required',
//            'author'                                                => 'required',
//            'approved_by'                                           => 'required',
//            'approved_at'                                           => 'required',
//            'rejected_by'                                           => 'required',
//            'rejected_at'                                           => 'required',
            'status'                                                => 'required',
            'category_id'                                           => 'required',
        ];
    }

    public function messages()
    {
        return [
            'slug.required'                                         => 'Bạn chưa nhập đường dẫn SEO',
            'title.required'                                        => 'Bạn chưa nhập tiêu đề',
            'keywords.required'                                     => 'Bạn chưa nhập từ khóa',
            'short_description.required'                            => 'Bạn chưa nhập mô tả ngắn',
            'description.required'                                  => 'Bạn chưa nhập mô tả',
            'thumbnails.required'                                   => 'Bạn chưa nhập hình ảnh',
//            'author.required'                                       => 'Bạn chưa nhập tài khoản',
//            'approved_by.required'                                  => 'Bạn chưa nhập người duyệt',
//            'approved_at.required'                                  => 'Bạn chưa nhập thời gian duyệt',
//            'rejected_by.required'                                  => 'Bạn chưa nhập người từ chối',
//            'rejected_at.required'                                  => 'Bạn chưa nhập thời gian từ chối',
            'status.required'                                       => 'Bạn chưa nhập trạng thái',
            'category_id.required'                                  => 'Bạn chưa nhập danh mục',
        ];
    }
}
