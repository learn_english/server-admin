<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class NotificationCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'slug'                                              => 'required',
            'title'                                             => 'required',
            'status'                                            => 'required',
        ];
    }

    public function messages()
    {
        return [
            'slug.required'                                     => 'Bạn chưa nhập đường dẫn SEO',
            'title.required'                                    => 'Bạn chưa nhập thông báo',
            'status.required'                                   => 'Bạn chưa nhập trạng thái',
        ];
    }
}
