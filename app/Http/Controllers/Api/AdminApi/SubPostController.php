<?php

namespace App\Http\Controllers;

use App\Http\Requests\SubPostCreateRequest;
use App\SubPost;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Cocur\Slugify\Slugify;

class SubPostController extends AbstractApiController
{
    public function index()
    {
        $subPost = SubPost::query()
            ->select([
                'id',
                'slug',
                'title',
                'keywords',
                'short_description',
                'description',
                'thumbnails',
                'pending_delete',
                'status',
                'category_id',
                'post_id',
            ])
            ->where('status','=', 1)
            ->get();

        return $this->item($subPost);
    }

    public function getPaginate(Request $request)
    {
        $subPost = SubPost::query()
            ->select([
                'id',
                'slug',
                'title',
                'keywords',
                'short_description',
                'description',
                'thumbnails',
                'pending_delete',
                'status',
                'category_id',
                'post_id',
            ])
            ->DataTablePaginate($request);

        return $this->item($subPost);
    }

    public function create(SubPostCreateRequest $request)
    {
        $validatedData = $request->validated();
        $slugify = new Slugify();
        $payload = [];

        $payload['slug']                                    = $slugify->slugify($validatedData['slug']);
        $payload['title']                                   = $validatedData['title'];
        $payload['keywords']                                = $validatedData['keywords'];
        $payload['short_description']                       = $validatedData['short_description'];
        $payload['description']                             = $validatedData['description'];
        $payload['thumbnails']                              = $validatedData['thumbnails'];

        $payload['status']                                  = $validatedData['status'];

        $payload['category_id']                             = $validatedData['category_id'];
        $payload['post_id']                                 = $validatedData['post_id'];


        // Kiểm tra trùng tên
        if (! $this->checkDuplicateName($payload['title'])) {
            $this->setMessage('Đã tồn tại tiêu đề');
            $this->setStatusCode(400);
            return $this->respond();
        }

        // Tạo và lưu
        $subPost = SubPost::query()->create($payload);
        DB::beginTransaction();

        try {
            $subPost->save();
            DB::commit();
            // Trả kết quả
            $this->setMessage('Thêm bài viết thành công!');
            $this->setStatusCode(200);
            $this->setData($subPost);

        } catch (Exception $e) {
            report($e);
            DB::rollBack();
            // Thông báo lỗi
            $this->setMessage($e->getMessage());
            $this->setStatusCode(500);
        }
        return $this->respond();
    }

    public function show($id)
    {
        return SubPost::query()->findOrFail($id);
    }

    public function update(SubPostCreateRequest $request, $id)
    {
        $validatedData = $request->validated();
        $slugify = new Slugify();
        $subPost = SubPost::query()->findOrFail($id);
        if (!$subPost) {
            $this->setMessage('Không có bài viết này!');
            $this->setStatusCode(400);
        } else {
            DB::beginTransaction();

            if($request->changeImages == 1)
            {
                File::delete('images/SubPost/'.$subPost->thumbnails);
            }

            try {
                // Cập nhật
                $subPost->slug                                     = $slugify->slugify($validatedData['slug']);
                $subPost->title                                    = $validatedData['title'];
                $subPost->keywords                                 = $validatedData['keywords'];
                $subPost->short_description                        = $validatedData['short_description'];
                $subPost->description                              = $validatedData['description'];
                $subPost->thumbnails                               = $validatedData['thumbnails'];

                $subPost->status                                   = $validatedData['status'];

                $subPost->category_id                              = $validatedData['category_id'];
                $subPost->post_id                                  = $validatedData['post_id'];

                $subPost->save();
                DB::commit();

                // Trả về kết quả
                $this->setMessage('Cập nhật thành công');
                $this->setStatusCode(200);
                $this->setData($subPost);
            } catch (Exception $e) {
                report($e);
                DB::rollBack();

                // Thông báo lỗi
                $this->setMessage($e->getMessage());
                $this->setStatusCode(500);
            }
        }
        return $this->respond();
    }

    public function remove($id)
    {
        $subPost = SubPost::query()->where('id', '=', $id);
        $subPost->delete();

        return response()
            ->json(['message' => 'Success: Bạn đã xóa thành công!']);
    }

    private function checkDuplicateName($title)
    {
        $subPost = SubPost::query()->get();
        foreach ($subPost->pluck('title') as $item) {
            if ($title == $item) {
                return false;
            }
        }
        return true;
    }

    public function searchAll(Request $request)
    {
        $search = $request->keyText;

        $subPost = SubPost::query()
            ->select([
                'id',
                'slug',
                'title',
                'keywords',
                'short_description',
                'description',
                'thumbnails',
                'pending_delete',
                'status',
                'category_id',
                'post_id',
            ])
            ->where('title', 'LIKE', "%$search%")
            ->where('status', '=', 1)
            ->DataTablePaginate($request);
        return $this->item($subPost);
    }

    public function checkActive($id, $status)
    {
        $subPost = SubPost::query()->findOrFail($id);
        if (!$subPost) {
            $this->setMessage('Không có bài viết này');
            $this->setStatusCode(400);
        } else {
            DB::beginTransaction();

            try {
                // Cập nhật trạng thái
                $subPost->status = $status;

                $subPost->save();
                DB::commit();

                // Trả về kết quả
                $this->setMessage('Cập nhật trạng thái thành công');
                $this->setStatusCode(200);
                $this->setData($subPost);
            } catch (Exception $e) {
                report($e);
                DB::rollBack();

                // Thông báo lỗi
                $this->setMessage($e->getMessage());
                $this->setStatusCode(500);
            }
        }
        return $this->respond();
    }

    public function upload(Request $request)
    {
        if ($request->hasFile('image')) {
            try {
                $Carbon = new Carbon();
                $theTime = Carbon::now()->format('Y-m-d');
                $theImageName = $theTime . '-' . $request->image->getClientOriginalName();
                $request->image->move(public_path('images/SubPost'), $theImageName);

                $this->setMessage('Thêm ảnh thành công!');
                $this->setStatusCode(200);
                $this->setData($theImageName);
                return $this->respond();
            }
            catch (Exception $e) {
                report($e);
                DB::rollBack();
                // Thông báo lỗi
                $this->setMessage($e->getMessage());
                $this->setStatusCode(500);
            }
        }
        return $this->respond();
    }
}
