<?php

namespace App\Http\Controllers;

use App\Http\Requests\SlideCreateRequest;
use App\Slide;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Cocur\Slugify\Slugify;

class SlideController extends AbstractApiController
{
    public function index()
    {
        $slide = Slide::query()
            ->select([
                'id',
                'slug',
                'title',
                'short_description',
                'thumbnails',
                'link',
                'status',
            ])
            ->where('status','=', 1)
            ->get();

        return $this->item($slide);
    }

    public function getPaginate(Request $request)
    {
        $slide = Slide::query()
            ->select([
                'id',
                'slug',
                'title',
                'short_description',
                'thumbnails',
                'link',
                'status',
            ])
            ->DataTablePaginate($request);

        return $this->item($slide);
    }

    public function create(SlideCreateRequest $request)
    {
        $validatedData = $request->validated();
        $slugify = new Slugify();
        $payload = [];

        $payload['slug']                                        = $slugify->slugify($validatedData['slug']);
        $payload['title']                                       = $validatedData['title'];
        $payload['short_description']                           = $validatedData['short_description'];
        $payload['thumbnails']                                  = $validatedData['thumbnails'];
        $payload['status']                                      = $validatedData['status'];
        $payload['link']                                        = $validatedData['link'];

        // Kiểm tra trùng tên
        if (! $this->checkDuplicateName($payload['title'])) {
            $this->setMessage('Đã tồn tại tiêu đề');
            $this->setStatusCode(400);
            return $this->respond();
        }

        // Tạo và lưu
        $slide = Slide::query()->create($payload);
        DB::beginTransaction();

        try {
            $slide->save();
            DB::commit();
            // Trả kết quả
            $this->setMessage('Thêm sldie thành công!');
            $this->setStatusCode(200);
            $this->setData($slide);
        } catch (Exception $e) {
            report($e);
            DB::rollBack();
            // Thông báo lỗi
            $this->setMessage($e->getMessage());
            $this->setStatusCode(500);
        }
        return $this->respond();
    }

    public function show($id)
    {
        return Slide::query()->findOrFail($id);
    }

    public function update(SlideCreateRequest $request, $id)
    {
        $validatedData = $request->validated();
        $slugify = new Slugify();
        $slide = Slide::query()->findOrFail($id);
        if (!$slide) {
            $this->setMessage('Không có slide này!');
            $this->setStatusCode(400);
        } else {
            DB::beginTransaction();

            if($request->changeImages == 1)
            {
                File::delete('images/Slide/'.$slide->thumbnails);
            }

            try {
                // Cập nhật
                $slide->slug                               = $slugify->slugify($validatedData['slug']);
                $slide->title                              = $validatedData['title'];
                $slide->short_description                  = $validatedData['short_description'];
                $slide->thumbnails                         = $validatedData['thumbnails'];
                $slide->status                             = $validatedData['status'];
                $slide->link                               = $validatedData['link'];

                $slide->save();
                DB::commit();

                // Trả về kết quả
                $this->setMessage('Cập nhật thành công');
                $this->setStatusCode(200);
                $this->setData($slide);
            } catch (Exception $e) {
                report($e);
                DB::rollBack();

                // Thông báo lỗi
                $this->setMessage($e->getMessage());
                $this->setStatusCode(500);
            }
        }
        return $this->respond();
    }

    public function remove($id)
    {
        $slide = Slide::query()->where('id', '=', $id);
        $slide->delete();

        return response()
            ->json(['message' => 'Success: Bạn đã xóa thành công!']);
    }

    private function checkDuplicateName($title)
    {
        $slide = Slide::query()->get();
        foreach ($slide->pluck('title') as $item) {
            if ($title == $item) {
                return false;
            }
        }
        return true;
    }

    public function searchAll(Request $request)
    {
        $search = $request->keyText;

        $slide = Slide::query()
            ->select([
                'id',
                'slug',
                'title',
                'short_description',
                'thumbnails',
                'link',
                'status',
            ])
            ->where('title', 'LIKE', "%$search%")
            ->where('status', '=', 1)
            ->DataTablePaginate($request);
        return $this->item($slide);
    }

    public function checkActive($id, $status)
    {
        $slide = Slide::query()->findOrFail($id);
        if (!$slide) {
            $this->setMessage('Không có slide này');
            $this->setStatusCode(400);
        } else {
            DB::beginTransaction();

            try {
                // Cập nhật trạng thái
                $slide->status = $status;

                $slide->save();
                DB::commit();

                // Trả về kết quả
                $this->setMessage('Cập nhật trạng thái thành công');
                $this->setStatusCode(200);
                $this->setData($slide);
            } catch (Exception $e) {
                report($e);
                DB::rollBack();

                // Thông báo lỗi
                $this->setMessage($e->getMessage());
                $this->setStatusCode(500);
            }
        }
        return $this->respond();
    }

    public function upload(Request $request)
    {
        if ($request->hasFile('image')) {
            try {
                $Carbon = new Carbon();
                $theTime = Carbon::now()->format('Y-m-d');
                $theImageName = $theTime . '-' . $request->image->getClientOriginalName();
                $request->image->move(public_path('images/Slide'), $theImageName);

                $this->setMessage('Thêm ảnh thành công!');
                $this->setStatusCode(200);
                $this->setData($theImageName);
                return $this->respond();
            }
            catch (Exception $e) {
                report($e);
                DB::rollBack();
                // Thông báo lỗi
                $this->setMessage($e->getMessage());
                $this->setStatusCode(500);
            }
        }
        return $this->respond();
    }
}
