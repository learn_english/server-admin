<?php

namespace App\Http\Controllers;

use App\Http\Requests\LiveCourseCreateRequest;
use App\LiveCourse;
use App\PostLiveCourse;
use App\TeachingCenter;
use App\TeachingCenterLiveCourse;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Cocur\Slugify\Slugify;

class LiveCourseController extends AbstractApiController
{
    public function index()
    {
        $liveCourse = LiveCourse::query()
            ->select([
                'id',
                'slug',
                'title',
//                'teaching_center_id',
                'price',
                'address',
                'keywords',
                'short_description',
                'description',
                'youtube',
//                'author',
//                'approved_by',
//                'approved_at',
//                'rejected_by',
//                'rejected_at',
                'pending_delete',
                'status',
            ])
            ->where('status','=', 1)
            ->get();

        return $this->item($liveCourse);
    }

    public function getPaginate(Request $request)
    {
        $liveCourse = LiveCourse::query()
            ->select([
                'id',
                'slug',
                'title',
//                'teaching_center_id',
                'price',
                'address',
                'description',
                'keywords',
                'short_description',
                'youtube',
//                'author',
//                'approved_by',
//                'approved_at',
//                'rejected_by',
//                'rejected_at',
                'pending_delete',
                'status',
            ])
            ->DataTablePaginate($request);

        return $this->item($liveCourse);
    }

    public function create(LiveCourseCreateRequest $request)
    {
        $validatedData = $request->validated();
        $slugify = new Slugify();
        $payload = [];

        $payload['slug']                                        = $slugify->slugify($validatedData['slug']);
        $payload['title']                                       = $validatedData['title'];
//        $payload['teaching_center_id']                          = $validatedData['teaching_center_id'];
        $payload['price']                                       = $validatedData['price'];
        $payload['address']                                     = $validatedData['address'];
        $payload['description']                                 = $validatedData['description'];
        $payload['keywords']                                    = $validatedData['keywords'];
        $payload['short_description']                           = $validatedData['short_description'];
        $payload['youtube']                                     = $validatedData['youtube'];
//        $payload['author'] = $validatedData['author'];
//        $payload['approved_by'] = $validatedData['approved_by'];
//        $payload['approved_at'] = $validatedData['approved_at'];
//        $payload['rejected_by'] = $validatedData['rejected_by'];
//        $payload['rejected_at'] = $validatedData['rejected_at'];
        $payload['status']                                      = $validatedData['status'];

        // Kiểm tra trùng tên
        if (! $this->checkDuplicateName($payload['title'])) {
            $this->setMessage('Đã tồn tại tiêu đề');
            $this->setStatusCode(400);
            return $this->respond();
        }

        // Tạo và lưu
        $liveCourse = LiveCourse::query()->create($payload);
        DB::beginTransaction();

        try {
            $liveCourse->save();
            DB::commit();
            // Trả kết quả
            $this->setMessage('Thêm bài viết thành công!');
            $this->setStatusCode(200);
            $this->setData($liveCourse);

            // Thêm mới vào bảng trung gian trung tâm - khóa học trực tiếp
            if(!empty($request['teaching_center_id']))
            {
                $payloadTeachingCenterLiveCourse = [];
                foreach ($request['teaching_center_id'] as $item) {
                    $payloadTeachingCenterLiveCourse['live_course_id']                    = $liveCourse->id;
                    $payloadTeachingCenterLiveCourse['teaching_center_id']                = $item['id'];

                    $teachingCenterLiveCourse = TeachingCenterLiveCourse::query()->create($payloadTeachingCenterLiveCourse);

                    $teachingCenterLiveCourse->save();
                    DB::commit();
                }
            }

        } catch (Exception $e) {
            report($e);
            DB::rollBack();
            // Thông báo lỗi
            $this->setMessage($e->getMessage());
            $this->setStatusCode(500);
        }
        return $this->respond();
    }

    public function show($id)
    {
        $liveCourse = LiveCourse::query()->findOrFail($id);

        $teachingCenterLiveCourse = TeachingCenterLiveCourse::query()
            ->where('live_course_id', '=', $liveCourse->id)
            ->get();

        $arrTeachingCenter = [];

        foreach ($teachingCenterLiveCourse as $item)
        {
            $teachingCenter = TeachingCenter::query()->where('id', '=', $item->teaching_center_id)->firstOrFail();
            array_push($arrTeachingCenter, $teachingCenter);
        }

        return $this->item([$liveCourse, $arrTeachingCenter]);
    }

    public function update(LiveCourseCreateRequest $request, $id)
    {
        $validatedData = $request->validated();
        $slugify = new Slugify();
        $liveCourse = LiveCourse::query()->findOrFail($id);
        if (!$liveCourse) {
            $this->setMessage('Không có bài viết này!');
            $this->setStatusCode(400);
        } else {
            DB::beginTransaction();

            try {
                // Cập nhật
                $liveCourse->slug                               = $slugify->slugify($validatedData['slug']);
                $liveCourse->title                              = $validatedData['title'];
                $liveCourse->price                              = $validatedData['price'];
                $liveCourse->address                            = $validatedData['address'];
                $liveCourse->description                        = $validatedData['description'];
                $liveCourse->keywords                           = $validatedData['keywords'];
                $liveCourse->short_description                  = $validatedData['short_description'];
                $liveCourse->youtube                            = $validatedData['youtube'];
                $liveCourse->status                             = $validatedData['status'];

                $liveCourse->save();
                DB::commit();

                // Trả về kết quả
                $this->setMessage('Cập nhật thành công');
                $this->setStatusCode(200);
                $this->setData($liveCourse);

                $payloadTeachingCenterLiveCourse = [];

                // Thêm mới vào bảng trung gian bài đăng - khóa học trực tiếp
                if(!empty($request['arrTeachingCenter']))
                {
                    $tclc = TeachingCenterLiveCourse::query()->where('live_course_id', '=', $liveCourse->id)->get();
                    foreach ($tclc as $itlive) {
                        $itlive->delete();
                    }

                    foreach ($request['arrTeachingCenter'] as $item) {
                        $payloadTeachingCenterLiveCourse['live_course_id']                    = $liveCourse->id;
                        $payloadTeachingCenterLiveCourse['teaching_center_id']                = $item['id'];

                        $teachingCenterLiveCourse = TeachingCenterLiveCourse::query()->create($payloadTeachingCenterLiveCourse);

                        $teachingCenterLiveCourse->save();
                        DB::commit();
                    }
                }
                else {
                    $tclc = TeachingCenterLiveCourse::query()->where('live_course_id', '=', $liveCourse->id)->get();
                    foreach ($tclc as $itlive) {
                        $itlive->delete();
                    }
                }

            } catch (Exception $e) {
                report($e);
                DB::rollBack();

                // Thông báo lỗi
                $this->setMessage($e->getMessage());
                $this->setStatusCode(500);
            }
        }
        return $this->respond();
    }

    public function remove($id)
    {
        $liveCourse = LiveCourse::query()->where('id', '=', $id);
        $liveCourse->delete();

        return response()
            ->json(['message' => 'Success: Bạn đã xóa thành công!']);
    }

    private function checkDuplicateName($title)
    {
        $liveCourse = LiveCourse::query()->get();
        foreach ($liveCourse->pluck('title') as $item) {
            if ($title == $item) {
                return false;
            }
        }
        return true;
    }

    public function searchAll(Request $request)
    {
        $search = $request->keyText;

        $liveCourse = LiveCourse::query()
            ->select([
                'id',
                'slug',
                'title',
//                'teaching_center_id',
                'price',
                'address',
                'description',
                'keywords',
                'short_description',
                'youtube',
//                'author',
//                'approved_by',
//                'approved_at',
//                'rejected_by',
//                'rejected_at',
                'pending_delete',
                'status',
            ])
            ->where('title', 'LIKE', "%$search%")
            ->where('status', '=', 1)
            ->DataTablePaginate($request);
        return $this->item($liveCourse);
    }

    public function checkActive($id, $status)
    {
        $liveCourse = LiveCourse::query()->findOrFail($id);
        if (!$liveCourse) {
            $this->setMessage('Không có bài viết này');
            $this->setStatusCode(400);
        } else {
            DB::beginTransaction();

            try {
                // Cập nhật trạng thái
                $liveCourse->status = $status;

                $liveCourse->save();
                DB::commit();

                // Trả về kết quả
                $this->setMessage('Cập nhật trạng thái thành công');
                $this->setStatusCode(200);
                $this->setData($liveCourse);
            } catch (Exception $e) {
                report($e);
                DB::rollBack();

                // Thông báo lỗi
                $this->setMessage($e->getMessage());
                $this->setStatusCode(500);
            }
        }
        return $this->respond();
    }

    public function upload(Request $request)
    {
        if ($request->hasFile('image')) {
            try {
                $Carbon = new Carbon();
                $theTime = Carbon::now()->format('Y-m-d');
                $theImageName = $theTime . '-' . $request->image->getClientOriginalName();
                $request->image->move(public_path('images/LiveCourse'), $theImageName);

                $this->setMessage('Thêm ảnh thành công!');
                $this->setStatusCode(200);
                $this->setData($theImageName);
                return $this->respond();
            }
            catch (Exception $e) {
                report($e);
                DB::rollBack();
                // Thông báo lỗi
                $this->setMessage($e->getMessage());
                $this->setStatusCode(500);
            }
        }
        return $this->respond();
    }
}
