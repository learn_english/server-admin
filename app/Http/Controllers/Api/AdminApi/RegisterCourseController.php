<?php

namespace App\Http\Controllers;

use App\RegisterCourse;
use Illuminate\Http\Request;

class RegisterCourseController extends AbstractApiController
{
    public function index()
    {
        $registerCourse = RegisterCourse::query()
            ->select([
                'id',
                'last_name',
                'mid_name',
                'first_name',
                'email',
                'mobile',
                'address',
                'sex',
                'teaching_center_id',
                'live_course_id',
                'last_name_child',
                'mid_name_child',
                'first_name_child',
                'birthday_child',
                'why_know',
            ])
            ->get();
        return response()->json($registerCourse, 200);
    }

    public function getPaginate(Request $request)
    {
        $registerCourse = RegisterCourse::query()
            ->select([
                'id',
                'last_name',
                'mid_name',
                'first_name',
                'email',
                'mobile',
                'address',
                'sex',
                'teaching_center_id',
                'live_course_id',
                'last_name_child',
                'mid_name_child',
                'first_name_child',
                'birthday_child',
                'why_know',
            ])
            ->DataTablePaginate($request);

        return $this->item($registerCourse);
    }

    public function show($id)
    {
        return RegisterCourse::query()->findOrFail($id);
    }

    public function remove($id)
    {
        $registerCourse = RegisterCourse::query()->where('id', '=', $id);
        $registerCourse->delete();

        return response()
            ->json(['message' => 'Success: Bạn đã xóa thành công!']);
    }

    public function searchAll(Request $request)
    {
        $search = $request->keyText;

        $registerCourse = RegisterCourse::query()
            ->select([
                'id',
                'last_name',
                'mid_name',
                'first_name',
                'email',
                'mobile',
                'address',
                'sex',
                'teaching_center_id',
                'live_course_id',
                'last_name_child',
                'mid_name_child',
                'first_name_child',
                'birthday_child',
                'why_know',
            ])
            ->where('last_name', 'LIKE', "%$search%")
            ->orWhere('mid_name', 'LIKE', "%$search%")
            ->orWhere('first_name', 'LIKE', "%$search%")
            ->orWhere('email', 'LIKE', "%$search%")
            ->orWhere('mobile', 'LIKE', "%$search%")
            ->DataTablePaginate($request);
        return $this->item($registerCourse);
    }
}
