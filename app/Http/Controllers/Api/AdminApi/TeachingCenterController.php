<?php

namespace App\Http\Controllers;

use App\Http\Requests\TeachingCenterCreateRequest;
use App\TeachingCenter;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Cocur\Slugify\Slugify;

class TeachingCenterController extends AbstractApiController
{
    public function index()
    {
        $teachingCenter = TeachingCenter::query()
            ->select([
                'id',
                'slug',
                'name',
                'keywords',
                'short_description',
                'info',
                'time_table',
                'contact_table',
                'equipment',
                'longitude',
                'latitude',
                'thumbnails',
                'contact_mobile',
                'contact_phone',
                'contact_email',
                'contact_address',
                'pending_delete',
                'status'
            ])
            ->where('status','=', 1)
            ->get();
        return response()->json($teachingCenter, 200);
    }

    public function getPaginate(Request $request)
    {
        $teachingCenter = TeachingCenter::query()
            ->select([
                'id',
                'slug',
                'name',
                'keywords',
                'short_description',
                'info',
                'time_table',
                'contact_table',
                'equipment',
                'longitude',
                'latitude',
                'thumbnails',
                'contact_mobile',
                'contact_phone',
                'contact_email',
                'contact_address',
                'pending_delete',
                'status'
            ])
            ->where('status','=', 1)
            ->DataTablePaginate($request);

        return $this->item($teachingCenter);
    }

    public function create(TeachingCenterCreateRequest $request)
    {
        $validatedData = $request->validated();
        $slugify = new Slugify();
        $payload = [];

        $payload['slug'] = $slugify->slugify($validatedData['slug']);
        $payload['name'] = $validatedData['name'];
        $payload['keywords'] = $validatedData['keywords'];
        $payload['short_description'] = $validatedData['short_description'];
        $payload['info'] = $validatedData['info'];
        $payload['time_table'] = $validatedData['time_table'];
        $payload['contact_table'] = $validatedData['contact_table'];
        $payload['equipment'] = $validatedData['equipment'];
        $payload['longitude'] = $validatedData['longitude'];
        $payload['latitude'] = $validatedData['latitude'];
        $payload['thumbnails'] = $validatedData['thumbnails'];
        $payload['contact_mobile'] = $validatedData['contact_mobile'];
        $payload['contact_phone'] = $validatedData['contact_phone'];
        $payload['contact_email'] = $validatedData['contact_email'];
        $payload['contact_address'] = $validatedData['contact_address'];
        $payload['status'] = $validatedData['status'];

        // Kiểm tra trùng tên
        if (! $this->checkDuplicateName($payload['name'])) {
            $this->setMessage('Đã tồn tại trung tâm');
            $this->setStatusCode(400);
            return $this->respond();
        }

        // Tạo và lưu danh mục
        $teachingCenter = TeachingCenter::query()->create($payload);
        DB::beginTransaction();

        try {
            $teachingCenter->save();
            DB::commit();
            // Trả kết quả
            $this->setMessage('Thêm thành công!');
            $this->setStatusCode(200);
            $this->setData($teachingCenter);
        } catch (Exception $e) {
            report($e);
            DB::rollBack();
            // Thông báo lỗi
            $this->setMessage($e->getMessage());
            $this->setStatusCode(500);
        }
        return $this->respond();
    }

    public function show($id)
    {
        return TeachingCenter::query()->findOrFail($id);
    }

    public function update(TeachingCenterCreateRequest $request, $id)
    {
        $validatedData = $request->validated();
        $slugify = new Slugify();
        $teachingCenter = TeachingCenter::query()->findOrFail($id);
        if (!$teachingCenter) {
            $this->setMessage('Không có trung tâm này');
            $this->setStatusCode(400);
        } else {
            DB::beginTransaction();

            if($request->changeImages == 1)
            {
                File::delete('images/TeachingCenter/'.$teachingCenter->thumbnails);
            }

            try {
                // Cập nhật tên danh mục
                $teachingCenter->slug = $slugify->slugify($validatedData['slug']);
                $teachingCenter->name = $validatedData['name'];
                $teachingCenter->keywords = $validatedData['keywords'];
                $teachingCenter->short_description = $validatedData['short_description'];
                $teachingCenter->info = $validatedData['info'];
                $teachingCenter->time_table = $validatedData['time_table'];
                $teachingCenter->contact_table = $validatedData['contact_table'];
                $teachingCenter->equipment = $validatedData['equipment'];
                $teachingCenter->longitude = $validatedData['longitude'];
                $teachingCenter->latitude = $validatedData['latitude'];
                $teachingCenter->thumbnails = $validatedData['thumbnails'];
                $teachingCenter->contact_mobile = $validatedData['contact_mobile'];
                $teachingCenter->contact_phone = $validatedData['contact_phone'];
                $teachingCenter->contact_email = $validatedData['contact_email'];
                $teachingCenter->contact_address = $validatedData['contact_address'];
                $teachingCenter->status = $validatedData['status'];

                $teachingCenter->save();
                DB::commit();

                // Trả về kết quả
                $this->setMessage('Cập nhật thành công');
                $this->setStatusCode(200);
                $this->setData($teachingCenter);
            } catch (Exception $e) {
                report($e);
                DB::rollBack();

                // Thông báo lỗi
                $this->setMessage($e->getMessage());
                $this->setStatusCode(500);
            }
        }
        return $this->respond();
    }

    public function remove($id)
    {
        $teachingCenter = TeachingCenter::query()->where('id', '=', $id);
        $teachingCenter->delete();

        return response()
            ->json(['message' => 'Success: Bạn đã xóa thành công!']);
    }

    private function checkDuplicateName($name)
    {
        $teachingCenter = TeachingCenter::query()->get();
        foreach ($teachingCenter->pluck('name') as $item) {
            if ($name == $item) {
                return false;
            }
        }
        return true;
    }

    public function searchAll(Request $request)
    {
        $search = $request->keyText;

        $teachingCenter = TeachingCenter::query()
            ->select([
                'id',
                'slug',
                'name',
                'keywords',
                'short_description',
                'info',
                'time_table',
                'contact_table',
                'equipment',
                'longitude',
                'latitude',
                'thumbnails',
                'contact_mobile',
                'contact_phone',
                'contact_email',
                'contact_address',
                'pending_delete',
                'status'
            ])
            ->where('status','=', 1)
            ->where('name', 'LIKE', "%$search%")
            ->where('status', '=', 1)
            ->DataTablePaginate($request);
        return $this->item($teachingCenter);
    }

    public function checkActive($id, $status)
    {
        $teachingCenter = TeachingCenter::query()->findOrFail($id);
        if (!$teachingCenter) {
            $this->setMessage('Không có đơn vị này');
            $this->setStatusCode(400);
        } else {
            DB::beginTransaction();

            try {
                // Cập nhật trạng thái
                $teachingCenter->status = $status;

                $teachingCenter->save();
                DB::commit();

                // Trả về kết quả
                $this->setMessage('Cập nhật trạng thái thành công');
                $this->setStatusCode(200);
                $this->setData($teachingCenter);
            } catch (Exception $e) {
                report($e);
                DB::rollBack();

                // Thông báo lỗi
                $this->setMessage($e->getMessage());
                $this->setStatusCode(500);
            }
        }
        return $this->respond();
    }

    public function upload(Request $request)
    {
        if ($request->hasFile('image')) {
            try {
                $Carbon = new Carbon();
                $theTime = Carbon::now()->format('Y-m-d');
                $theImageName = $theTime . '-' . $request->image->getClientOriginalName();
                $request->image->move(public_path('images/TeachingCenter'), $theImageName);

                $this->setMessage('Thêm ảnh thành công!');
                $this->setStatusCode(200);
                $this->setData($theImageName);
                return $this->respond();
            }
            catch (Exception $e) {
                report($e);
                DB::rollBack();
                // Thông báo lỗi
                $this->setMessage($e->getMessage());
                $this->setStatusCode(500);
            }
        }
        return $this->respond();
    }
}
