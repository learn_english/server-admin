<?php

namespace App\Http\Controllers;
use App\Http\Requests\UserCreateRequest;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class UserController extends AbstractApiController
{
    public function index()
    {
        return response()->json(User::query()->get(), 200);
    }

    public function getPaginate(Request $request)
    {
        $User = User::query()
            ->select([
                'id',
                'username',
                'password',
                'address',
                'mobile',
                'email',
                'email_verified_at',
                'identity_number',
                'last_name',
                'first_name',
                'sex',
                'role',
                'status',
            ])
            ->DataTablePaginate($request);

        return $this->item($User);
    }

    public function isActive()
    {
        foreach (User::all() as $us) {
            if($us->isOnline()) {
                return response()->json('ok nha', 200);
            }
            return response()->json('ko nha', 200);
        }
    }

    public function create(UserCreateRequest $request)
    {
        $validatedData = $request->validated();

        $payload = [];

        $payload['username']                            = $validatedData['username'];
        $payload['password']                            = bcrypt($validatedData['password']);
        $payload['mobile']                              = $validatedData['mobile'];
        $payload['address']                             = $validatedData['address'];
        $payload['email']                               = $validatedData['email'];
        $payload['email_verified_at']                   = $validatedData['email_verified_at'];
        $payload['identity_number']                     = $validatedData['identity_number'];
        $payload['last_name']                           = $validatedData['last_name'];
        $payload['first_name']                          = $validatedData['first_name'];
        $payload['sex']                                 = $validatedData['sex'];
        $payload['role']                                = $validatedData['role'];
        $payload['status']                              = $validatedData['status'];

        // Kiểm tra trùng tên danh mục
        if (! $this->checkDuplicateName($payload['username'])) {
            $this->setMessage('Đã tồn tại tên tài khoản');
            $this->setStatusCode(400);
            return $this->respond();
        }

        // Tạo và lưu tài khoản
        $User = User::create($payload);
        DB::beginTransaction();

        try {
            $User->save();
            DB::commit();
            // Trả kết quả
            $this->setMessage('Thêm tài khoản thành công!');
            $this->setStatusCode(200);
            $this->setData($User);
        } catch (Exception $e) {
            report($e);
            DB::rollBack();
            // Thông báo lỗi
            $this->setMessage($e->getMessage());
            $this->setStatusCode(500);
        }
        return $this->respond();
    }

    public function show($id)
    {
        return User::query()->findOrFail($id);
    }

    public function update(UserCreateRequest $request, $id)
    {
        $validatedData = $request->validated();

        $User = User::query()->findOrFail($id);
        if (! $User) {
            $this->setMessage('Không có tài khoản này');
            $this->setStatusCode(400);
        } else {
            DB::beginTransaction();

            try {
                // Cập nhật tên tài khoản
                $User->username                                     = $validatedData['username'];
                $User->password                                     = bcrypt($validatedData['password']);
                $User->mobile                                       = $validatedData['mobile'];
                $User->address                                      = $validatedData['address'];
                $User->email                                        = $validatedData['email'];
                $User->email_verified_at                            = $validatedData['email_verified_at'];
                $User->identity_number                              = $validatedData['identity_number'];
                $User->last_name                                    = $validatedData['last_name'];
                $User->first_name                                   = $validatedData['first_name'];
                $User->sex                                          = $validatedData['sex'];
                $User->role                                         = $validatedData['role'];
                $User->status                                       = $validatedData['status'];

                $User->save();
                DB::commit();

                // Trả về kết quả
                $this->setMessage('Cập nhật thành công');
                $this->setStatusCode(200);
                $this->setData($User);
            } catch (Exception $e) {
                report($e);
                DB::rollBack();

                // Thông báo lỗi
                $this->setMessage($e->getMessage());
                $this->setStatusCode(500);
            }
        }
        return $this->respond();
    }

    public function remove($id)
    {
        $User = User::query()->where('id', '=', $id);
        $User->delete();

        return response()
            ->json(['message' => 'Success: Bạn đã xóa thành công!']);
    }

    /**
     * Kiểm tra trùng tên. Nếu trùng trả về false
     *
     * @param mixed $name
     */
    private function checkDuplicateName($username)
    {
        $User = User::query()->get();
        foreach ($User->pluck('username') as $item) {
            if ($username == $item) {
                return false;
            }
        }
        return true;
    }

    public function searchAll(Request $request)
    {
        $search = $request->keyText;

        $User = User::query()
            ->select([
                'id',
                'username',
                'password',
                'address',
                'mobile',
                'email',
                'email_verified_at',
                'identity_number',
                'last_name',
                'first_name',
                'sex',
                'role',
                'status',
            ])
            ->where('username', 'LIKE', "%$search%")
            ->orWhere('identity_number', 'LIKE', "%$search%")
            ->orWhere('email', 'LIKE', "%$search%")
            ->DataTablePaginate($request);
        return $this->item($User);
    }

    public function checkActive($id, $status)
    {
        $User = User::query()->findOrFail($id);
        if (! $User) {
            $this->setMessage('Không có tài khoản này');
            $this->setStatusCode(400);
        } else {
            DB::beginTransaction();

            try {
                // Cập nhật trạng thái tài khoản
                $User->status                       = $status;

                $User->save();
                DB::commit();

                // Trả về kết quả
                $this->setMessage('Cập nhật trạng thái tài khoản thành công');
                $this->setStatusCode(200);
                $this->setData($User);
            } catch (Exception $e) {
                report($e);
                DB::rollBack();

                // Thông báo lỗi
                $this->setMessage($e->getMessage());
                $this->setStatusCode(500);
            }
        }
        return $this->respond();
    }
}
