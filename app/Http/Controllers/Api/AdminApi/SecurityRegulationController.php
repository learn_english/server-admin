<?php

namespace App\Http\Controllers;

use App\Http\Requests\SecurityRegulationCreateRequest;
use App\SecurityRegulation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Cocur\Slugify\Slugify;

class SecurityRegulationController extends AbstractApiController
{
    public function index()
    {
        $securityRegulation = SecurityRegulation::query()
            ->select([
                'id',
                'slug',
                'title',
                'description',
                'status'
            ])
            ->where('status','=', 1)
            ->get();
        return response()->json($securityRegulation, 200);
    }

    public function getPaginate(Request $request)
    {
        $securityRegulation = SecurityRegulation::query()
            ->select([
                'id',
                'slug',
                'title',
                'description',
                'status'
            ])
            ->where('status','=', 1)
            ->DataTablePaginate($request);

        return $this->item($securityRegulation);
    }

    public function create(SecurityRegulationCreateRequest $request)
    {
        $validatedData = $request->validated();
        $slugify = new Slugify();
        $payload = [];

        $payload['slug']                                        = $slugify->slugify($validatedData['slug']);
        $payload['title']                                       = $validatedData['title'];
        $payload['description']                                 = $validatedData['description'];
        $payload['status']                                      = $validatedData['status'];

        // Kiểm tra trùng tên
        if (! $this->checkDuplicateName($payload['title'])) {
            $this->setMessage('Đã tồn tại tiêu đề');
            $this->setStatusCode(400);
            return $this->respond();
        }

        // Tạo và lưu danh mục
        $securityRegulation = SecurityRegulation::query()->create($payload);
        DB::beginTransaction();

        try {
            $securityRegulation->save();
            DB::commit();
            // Trả kết quả
            $this->setMessage('Thêm chính sách bảo mật thành công!');
            $this->setStatusCode(200);
            $this->setData($securityRegulation);
        } catch (Exception $e) {
            report($e);
            DB::rollBack();
            // Thông báo lỗi
            $this->setMessage($e->getMessage());
            $this->setStatusCode(500);
        }
        return $this->respond();
    }

    public function show($id)
    {
        return SecurityRegulation::query()->findOrFail($id);
    }

    public function update(SecurityRegulationCreateRequest $request, $id)
    {
        $validatedData = $request->validated();
        $slugify = new Slugify();
        $securityRegulation = SecurityRegulation::query()->findOrFail($id);
        if (!$securityRegulation) {
            $this->setMessage('Không có chính sách bảo mật này');
            $this->setStatusCode(400);
        } else {
            DB::beginTransaction();

            try {
                // Cập nhật tên danh mục
                $securityRegulation->slug                                   = $slugify->slugify($validatedData['slug']);
                $securityRegulation->title                                  = $validatedData['title'];
                $securityRegulation->description                            = $validatedData['description'];
                $securityRegulation->status                                 = $validatedData['status'];

                $securityRegulation->save();
                DB::commit();

                // Trả về kết quả
                $this->setMessage('Cập nhật thành công');
                $this->setStatusCode(200);
                $this->setData($securityRegulation);
            } catch (Exception $e) {
                report($e);
                DB::rollBack();

                // Thông báo lỗi
                $this->setMessage($e->getMessage());
                $this->setStatusCode(500);
            }
        }
        return $this->respond();
    }

    public function remove($id)
    {
        $securityRegulation = SecurityRegulation::query()->where('id', '=', $id);
        $securityRegulation->delete();

        return response()
            ->json(['message' => 'Success: Bạn đã xóa thành công!']);
    }

    private function checkDuplicateName($title)
    {
        $securityRegulation = SecurityRegulation::query()->get();
        foreach ($securityRegulation->pluck('title') as $item) {
            if ($title == $item) {
                return false;
            }
        }
        return true;
    }

    public function searchAll(Request $request)
    {
        $search = $request->keyText;

        $securityRegulation = SecurityRegulation::query()
            ->select([
                'id',
                'slug',
                'title',
                'description',
                'status'
            ])
            ->where('status','=', 1)
            ->where('title', 'LIKE', "%$search%")
            ->DataTablePaginate($request);
        return $this->item($securityRegulation);
    }

    public function checkActive($id, $status)
    {
        $securityRegulation = SecurityRegulation::query()->findOrFail($id);
        if (!$securityRegulation) {
            $this->setMessage('Không có chính sách này');
            $this->setStatusCode(400);
        } else {
            DB::beginTransaction();

            try {
                // Cập nhật trạng thái
                $securityRegulation->status = $status;

                $securityRegulation->save();
                DB::commit();

                // Trả về kết quả
                $this->setMessage('Cập nhật trạng thái thành công');
                $this->setStatusCode(200);
                $this->setData($securityRegulation);
            } catch (Exception $e) {
                report($e);
                DB::rollBack();

                // Thông báo lỗi
                $this->setMessage($e->getMessage());
                $this->setStatusCode(500);
            }
        }
        return $this->respond();
    }
}
