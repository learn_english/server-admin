<?php

namespace App\Http\Controllers;

use App\Newsletter;
use Illuminate\Http\Request;

class NewsletterController extends AbstractApiController
{
    public function index()
    {
        $newsletter = Newsletter::query()
            ->select([
                'id',
                'full_name',
                'email',
                'mobile',
                'address',
                'sex'
            ])
            ->get();
        return response()->json($newsletter, 200);
    }

    public function getPaginate(Request $request)
    {
        $newsletter = Newsletter::query()
            ->select([
                'id',
                'full_name',
                'email',
                'mobile',
                'address',
                'sex'
            ])
            ->DataTablePaginate($request);

        return $this->item($newsletter);
    }

    public function show($id)
    {
        return Newsletter::query()->findOrFail($id);
    }

    public function remove($id)
    {
        $newsletter = Newsletter::query()->where('id', '=', $id);
        $newsletter->delete();

        return response()
            ->json(['message' => 'Success: Bạn đã xóa thành công!']);
    }

    public function searchAll(Request $request)
    {
        $search = $request->keyText;

        $newsletter = Newsletter::query()
            ->select([
                'id',
                'full_name',
                'email',
                'mobile',
                'address',
                'sex'
            ])
            ->where('email', 'LIKE', "%$search%")
            ->orWhere('mobile', 'LIKE', "%$search%")
            ->orWhere('address', 'LIKE', "%$search%")
            ->orWhere('full_name', 'LIKE', "%$search%")
            ->DataTablePaginate($request);
        return $this->item($newsletter);
    }
}
