<?php

namespace App\Http\Controllers;

use App\AppCourse;
use App\Http\Requests\PostCreateRequest;
use App\LiveCourse;
use App\OnlineCourse;
use App\Post;
use App\PostAppCourse;
use App\PostLiveCourse;
use App\PostOnlineCourse;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Cocur\Slugify\Slugify;

class PostController extends AbstractApiController
{
    public function index()
    {
        $post = Post::query()
            ->select([
                'id',
                'slug',
                'title',
                'keywords',
                'short_description',
                'description',
                'thumbnails',
//                'author',
//                'approved_by',
//                'approved_at',
//                'rejected_by',
//                'rejected_at',
                'pending_delete',
                'status',
                'category_id',
            ])
            ->where('status','=', 1)
            ->get();

        return $this->item($post);
    }

    public function getPaginate(Request $request)
    {
        $post = Post::query()
            ->select([
                'id',
                'slug',
                'title',
                'keywords',
                'short_description',
                'description',
                'thumbnails',
//                'author',
//                'approved_by',
//                'approved_at',
//                'rejected_by',
//                'rejected_at',
                'pending_delete',
                'status',
                'category_id',
            ])
            ->DataTablePaginate($request);

        return $this->item($post);
    }

    public function getPosts($id)
    {
        $post = Post::query()
            ->select([
                'id',
                'slug',
                'title',
                'keywords',
                'short_description',
                'description',
                'thumbnails',
                'pending_delete',
                'status',
                'category_id',
            ])
            ->where('category_id', '=', $id)
            ->where('status', '=', 1)
            ->get();

        return $this->item($post);
    }

    public function bundle($id)
    {
        $query = Post::query();
        $query->select([
            'id',
            'slug',
            'title',
            'keywords',
            'short_description',
            'description',
            'thumbnails',
            'pending_delete',
            'status',
            'category_id',
        ]);
        $query->where('category_id', '=', $id);

        $post = $query->firstOrFail();

        return $this->item($post);
    }

    public function create(PostCreateRequest $request)
    {
        $validatedData = $request->validated();
        $slugify = new Slugify();
        $payload = [];

        $payload['slug']                                    = $slugify->slugify($validatedData['slug']);
        $payload['title']                                   = $validatedData['title'];
        $payload['keywords']                                = $validatedData['keywords'];
        $payload['short_description']                       = $validatedData['short_description'];
        $payload['description']                             = $validatedData['description'];
        $payload['thumbnails']                              = $validatedData['thumbnails'];
//        $payload['author']                                  = 1;
//        $payload['approved_by']                             = null;
//        $payload['approved_at']                             = null;
//        $payload['rejected_by']                             = null;
//        $payload['rejected_at']                             = null;

        $payload['status']                                  = $validatedData['status'];
        $payload['category_id']                             = $validatedData['category_id'];


        // Kiểm tra trùng tên
        if (! $this->checkDuplicateName($payload['title'])) {
            $this->setMessage('Đã tồn tại tiêu đề');
            $this->setStatusCode(400);
            return $this->respond();
        }

        // Tạo và lưu
        $post = Post::query()->create($payload);
        DB::beginTransaction();

        try {
            $post->save();
            DB::commit();
            // Trả kết quả
            $this->setMessage('Thêm bài viết thành công!');
            $this->setStatusCode(200);
            $this->setData($post);

            // Thêm mới vào bảng trung gian bài đăng - khóa học trực tiếp
            if(!empty($request['live_course_id']))
            {
                $payloadPostLiveCourse = [];
                foreach ($request['live_course_id'] as $item) {
                    $payloadPostLiveCourse['post_id']                    = $post->id;
                    $payloadPostLiveCourse['live_course_id']             = $item['id'];

                    $postLiveCourse = PostLiveCourse::query()->create($payloadPostLiveCourse);

                    $postLiveCourse->save();
                    DB::commit();
                }
            }
            // Thêm mới vào bảng trung gian bài đăng - khóa học online
            if(!empty($request['online_course_id']))
            {
                $payloadPostOnlineCourse = [];
                foreach ($request['online_course_id'] as $item) {
                    $payloadPostOnlineCourse['post_id']                         = $post->id;
                    $payloadPostOnlineCourse['online_course_id']                = $item['id'];

                    $postOnlineCourse = PostOnlineCourse::query()->create($payloadPostOnlineCourse);

                    $postOnlineCourse->save();
                    DB::commit();
                }
            }

            // Thêm mới vào bảng trung gian bài đăng - khóa học qua App
            if(!empty($request['app_course_id']))
            {
                $payloadPostAppCourse = [];
                foreach ($request['app_course_id'] as $item) {
                    $payloadPostAppCourse['post_id']                    = $post->id;
                    $payloadPostAppCourse['app_course_id']              = $item['id'];

                    $postAppCourse = PostAppCourse::query()->create($payloadPostAppCourse);

                    $postAppCourse->save();
                    DB::commit();
                }
            }
        } catch (Exception $e) {
            report($e);
            DB::rollBack();
            // Thông báo lỗi
            $this->setMessage($e->getMessage());
            $this->setStatusCode(500);
        }
        return $this->respond();
    }

    public function show($id)
    {
        $post = Post::query()->findOrFail($id);
        $postLiveCourse = PostLiveCourse::query()->where('post_id', '=', $post->id)->get();
        $postOnlineCourse = PostOnlineCourse::query()->where('post_id', '=', $post->id)->get();
        $postAppCourse = PostAppCourse::query()->where('post_id', '=', $post->id)->get();

        $arrLiveCourse = [];
        $arrOnlineCourse = [];
        $arrAppCourse = [];

        foreach ($postLiveCourse as $item1)
        {
            $liveCourse = LiveCourse::query()->where('id', '=', $item1->live_course_id)->firstOrFail();
            array_push($arrLiveCourse, $liveCourse);
        }

        foreach ($postOnlineCourse as $item2)
        {
            $onlineCourse = OnlineCourse::query()->where('id', '=', $item2->online_course_id)->firstOrFail();
            array_push($arrOnlineCourse, $onlineCourse);
        }

        foreach ($postAppCourse as $item3)
        {
            $appCourse = AppCourse::query()->where('id', '=', $item3->app_course_id)->firstOrFail();
            array_push($arrAppCourse, $appCourse);
        }


        return $this->item([$post, $arrLiveCourse, $arrOnlineCourse, $arrAppCourse]);
    }

    public function update(PostCreateRequest $request, $id)
    {
        $validatedData = $request->validated();
        $slugify = new Slugify();
        $post = Post::query()->findOrFail($id);
        if (!$post) {
            $this->setMessage('Không có bài viết này!');
            $this->setStatusCode(400);
        } else {
            DB::beginTransaction();

            if($request->changeImages == 1)
            {
                File::delete('images/Post/'.$post->thumbnails);
            }

            try {
                // Cập nhật
                $post->slug                                     = $slugify->slugify($validatedData['slug']);
                $post->title                                    = $validatedData['title'];
                $post->keywords                                 = $validatedData['keywords'];
                $post->short_description                        = $validatedData['short_description'];
                $post->description                              = $validatedData['description'];
                $post->thumbnails                               = $validatedData['thumbnails'];
//                $post->author                                   = $validatedData['author'];
//                $post->approved_by                              = $validatedData['approved_by'];
//                $post->approved_at                              = $validatedData['approved_at'];
//                $post->rejected_by                              = $validatedData['rejected_by'];
//                $post->rejected_at                              = $validatedData['rejected_at'];

                $post->status                                   = $validatedData['status'];

                $post->category_id                              = $validatedData['category_id'];
//                $post->sub_category_id                          = $validatedData['sub_category_id'];

                $post->save();
                DB::commit();

                // Trả về kết quả
                $this->setMessage('Cập nhật thành công');
                $this->setStatusCode(200);
                $this->setData($post);

                $payloadPostLiveCourse = [];
                $payloadPostOnlineCourse = [];
                $payloadPostAppCourse = [];


                // Thêm mới vào bảng trung gian bài đăng - khóa học trực tiếp
                if(!empty($request['arrLiveCourse']))
                {
                    $plc = PostLiveCourse::query()->where('post_id', '=', $post->id)->get();
                    foreach ($plc as $itlive) {
                        $itlive->delete();
                    }

                    foreach ($request['arrLiveCourse'] as $item) {
                        $payloadPostLiveCourse['post_id']                    = $post->id;
                        $payloadPostLiveCourse['live_course_id']             = $item['id'];

                        $postLiveCourse = PostLiveCourse::query()->create($payloadPostLiveCourse);

                        $postLiveCourse->save();
                        DB::commit();
                    }
                }
                else {
                    $plc = PostLiveCourse::query()->where('post_id', '=', $post->id)->get();
                    foreach ($plc as $itlive) {
                        $itlive->delete();
                    }
                }

                // Thêm mới vào bảng trung gian bài đăng - khóa học trực tiếp
                if(!empty($request['arrOnlineCourse']))
                {
                    $poc = PostOnlineCourse::query()->where('post_id', '=', $post->id)->get();
                    foreach ($poc as $itonline) {
                        $itonline->delete();
                    }

                    foreach ($request['arrOnlineCourse'] as $item) {
                        $payloadPostOnlineCourse['post_id']                         = $post->id;
                        $payloadPostOnlineCourse['online_course_id']                = $item['id'];

                        $postOnlineCourse = PostOnlineCourse::query()->create($payloadPostOnlineCourse);

                        $postOnlineCourse->save();
                        DB::commit();
                    }
                }
                else {
                    $poc = PostOnlineCourse::query()->where('post_id', '=', $post->id)->get();
                    foreach ($poc as $itonline) {
                        $itonline->delete();
                    }
                }

                // Thêm mới vào bảng trung gian bài đăng - khóa học trực tiếp
                if(!empty($request['arrAppCourse']))
                {
                    $pac = PostAppCourse::query()->where('post_id', '=', $post->id)->get();
                    foreach ($pac as $itapp) {
                        $itapp->delete();
                    }

                    foreach ($request['arrAppCourse'] as $item) {
                        $payloadPostAppCourse['post_id']                        = $post->id;
                        $payloadPostAppCourse['app_course_id']                  = $item['id'];

                        $postAppCourse = PostAppCourse::query()->create($payloadPostAppCourse);

                        $postAppCourse->save();
                        DB::commit();
                    }
                }
                else {
                    $pac = PostAppCourse::query()->where('post_id', '=', $post->id)->get();
                    foreach ($pac as $itapp) {
                        $itapp->delete();
                    }
                }

            } catch (Exception $e) {
                report($e);
                DB::rollBack();

                // Thông báo lỗi
                $this->setMessage($e->getMessage());
                $this->setStatusCode(500);
            }
        }
        return $this->respond();
    }

    public function remove($id)
    {
        $post = Post::query()->where('id', '=', $id);
        $post->delete();

        return response()
            ->json(['message' => 'Success: Bạn đã xóa thành công!']);
    }

    private function checkDuplicateName($title)
    {
        $post = Post::query()->get();
        foreach ($post->pluck('title') as $item) {
            if ($title == $item) {
                return false;
            }
        }
        return true;
    }

    public function searchAll(Request $request)
    {
        $search = $request->keyText;

        $post = Post::query()
            ->select([
                'id',
                'slug',
                'title',
                'keywords',
                'short_description',
                'description',
                'thumbnails',
//                'author',
//                'approved_by',
//                'approved_at',
//                'rejected_by',
//                'rejected_at',
                'pending_delete',
                'status',
                'category_id',
//                'sub_category_id',
            ])
            ->where('title', 'LIKE', "%$search%")
            ->where('status', '=', 1)
            ->DataTablePaginate($request);
        return $this->item($post);
    }

    public function checkActive($id, $status)
    {
        $post = Post::query()->findOrFail($id);
        if (!$post) {
            $this->setMessage('Không có bài viết này');
            $this->setStatusCode(400);
        } else {
            DB::beginTransaction();

            try {
                // Cập nhật trạng thái
                $post->status = $status;

                $post->save();
                DB::commit();

                // Trả về kết quả
                $this->setMessage('Cập nhật trạng thái thành công');
                $this->setStatusCode(200);
                $this->setData($post);
            } catch (Exception $e) {
                report($e);
                DB::rollBack();

                // Thông báo lỗi
                $this->setMessage($e->getMessage());
                $this->setStatusCode(500);
            }
        }
        return $this->respond();
    }

    public function upload(Request $request)
    {
        if ($request->hasFile('image')) {
            try {
                $Carbon = new Carbon();
                $theTime = Carbon::now()->format('Y-m-d');
                $theImageName = $theTime . '-' . $request->image->getClientOriginalName();
                $request->image->move(public_path('images/Post'), $theImageName);

                $this->setMessage('Thêm ảnh thành công!');
                $this->setStatusCode(200);
                $this->setData($theImageName);
                return $this->respond();
            }
            catch (Exception $e) {
                report($e);
                DB::rollBack();
                // Thông báo lỗi
                $this->setMessage($e->getMessage());
                $this->setStatusCode(500);
            }
        }
        return $this->respond();
    }
}
