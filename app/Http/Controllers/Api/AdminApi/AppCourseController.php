<?php

namespace App\Http\Controllers;

use App\AppCourse;
use App\Http\Requests\AppCourseCreateRequest;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Cocur\Slugify\Slugify;

class AppCourseController extends AbstractApiController
{
    public function index()
    {
        $appCourse = AppCourse::query()
            ->select([
                'id',
                'slug',
                'title',
                'price',
                'keywords',
                'short_description',
                'description',
                'thumbnails',
                'type_app',
                'pending_delete',
                'status',
            ])
            ->where('status','=', 1)
            ->get();

        return $this->item($appCourse);
    }

    public function getPaginate(Request $request)
    {
        $appCourse = AppCourse::query()
            ->select([
                'id',
                'slug',
                'title',
                'price',
                'keywords',
                'short_description',
                'description',
                'thumbnails',
                'type_app',
                'pending_delete',
                'status',
            ])
            ->DataTablePaginate($request);

        return $this->item($appCourse);
    }

    public function create(AppCourseCreateRequest $request)
    {
        $validatedData = $request->validated();
        $slugify = new Slugify();
        $payload = [];

        $payload['slug']                                        = $slugify->slugify($validatedData['slug']);
        $payload['title']                                       = $validatedData['title'];
        $payload['price']                                       = $validatedData['price'];
        $payload['description']                                 = $validatedData['description'];
        $payload['keywords']                                    = $validatedData['keywords'];
        $payload['short_description']                           = $validatedData['short_description'];
        $payload['thumbnails']                                  = $validatedData['thumbnails'];
        $payload['type_app']                                    = $validatedData['type_app'];
        $payload['status']                                      = $validatedData['status'];

        // Kiểm tra trùng tên
        if (! $this->checkDuplicateName($payload['title'])) {
            $this->setMessage('Đã tồn tại tiêu đề');
            $this->setStatusCode(400);
            return $this->respond();
        }

        // Tạo và lưu
        $appCourse = AppCourse::query()->create($payload);
        DB::beginTransaction();

        try {
            $appCourse->save();
            DB::commit();
            // Trả kết quả
            $this->setMessage('Thêm bài viết thành công!');
            $this->setStatusCode(200);
            $this->setData($appCourse);
        } catch (Exception $e) {
            report($e);
            DB::rollBack();
            // Thông báo lỗi
            $this->setMessage($e->getMessage());
            $this->setStatusCode(500);
        }
        return $this->respond();
    }

    public function show($id)
    {
        return AppCourse::query()->findOrFail($id);
    }

    public function update(AppCourseCreateRequest $request, $id)
    {
        $validatedData = $request->validated();
        $slugify = new Slugify();
        $appCourse = AppCourse::query()->findOrFail($id);
        if (!$appCourse) {
            $this->setMessage('Không có bài viết này!');
            $this->setStatusCode(400);
        } else {
            DB::beginTransaction();

            if($request->changeImages == 1)
            {
                File::delete('images/AppCourse/'.$appCourse->thumbnails);
            }

            try {
                // Cập nhật
                $appCourse->slug                               = $slugify->slugify($validatedData['slug']);
                $appCourse->title                              = $validatedData['title'];
                $appCourse->price                              = $validatedData['price'];
                $appCourse->description                        = $validatedData['description'];
                $appCourse->keywords                           = $validatedData['keywords'];
                $appCourse->short_description                  = $validatedData['short_description'];
                $appCourse->thumbnails                         = $validatedData['thumbnails'];
                $appCourse->type_app                           = $validatedData['type_app'];
                $appCourse->status                             = $validatedData['status'];

                $appCourse->save();
                DB::commit();

                // Trả về kết quả
                $this->setMessage('Cập nhật thành công');
                $this->setStatusCode(200);
                $this->setData($appCourse->thumbnails);
            } catch (Exception $e) {
                report($e);
                DB::rollBack();

                // Thông báo lỗi
                $this->setMessage($e->getMessage());
                $this->setStatusCode(500);
            }
        }
        return $this->respond();
    }

    public function remove($id)
    {
        $appCourse = AppCourse::query()->where('id', '=', $id);
        $appCourse->delete();

        return response()
            ->json(['message' => 'Success: Bạn đã xóa thành công!']);
    }

    private function checkDuplicateName($title)
    {
        $appCourse = AppCourse::query()->get();
        foreach ($appCourse->pluck('title') as $item) {
            if ($title == $item) {
                return false;
            }
        }
        return true;
    }

    public function searchAll(Request $request)
    {
        $search = $request->keyText;

        $appCourse = AppCourse::query()
            ->select([
                'id',
                'slug',
                'title',
                'price',
                'keywords',
                'short_description',
                'description',
                'thumbnails',
                'type_app',
                'pending_delete',
                'status',
            ])
            ->where('title', 'LIKE', "%$search%")
            ->where('status', '=', 1)
            ->DataTablePaginate($request);
        return $this->item($appCourse);
    }

    public function checkActive($id, $status)
    {
        $appCourse = AppCourse::query()->findOrFail($id);
        if (!$appCourse) {
            $this->setMessage('Không có bài viết này');
            $this->setStatusCode(400);
        } else {
            DB::beginTransaction();

            try {
                // Cập nhật trạng thái
                $appCourse->status = $status;

                $appCourse->save();
                DB::commit();

                // Trả về kết quả
                $this->setMessage('Cập nhật trạng thái thành công');
                $this->setStatusCode(200);
                $this->setData($appCourse);
            } catch (Exception $e) {
                report($e);
                DB::rollBack();

                // Thông báo lỗi
                $this->setMessage($e->getMessage());
                $this->setStatusCode(500);
            }
        }
        return $this->respond();
    }

    public function upload(Request $request)
    {
        if ($request->hasFile('image')) {
            try {
                $Carbon = new Carbon();
                $theTime = Carbon::now()->format('Y-m-d');
                $theImageName = $theTime . '-' . $request->image->getClientOriginalName();
                $request->image->move(public_path('images/AppCourse'), $theImageName);

                $this->setMessage('Thêm ảnh thành công!');
                $this->setStatusCode(200);
                $this->setData($theImageName);
                return $this->respond();
            }
            catch (Exception $e) {
                report($e);
                DB::rollBack();
                // Thông báo lỗi
                $this->setMessage($e->getMessage());
                $this->setStatusCode(500);
            }
        }
        return $this->respond();
    }
}
