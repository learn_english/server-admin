<?php

namespace App\Http\Controllers;

use App\Banner;
use App\Http\Requests\BannerCreateRequest;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Cocur\Slugify\Slugify;

class BannerController extends AbstractApiController
{
    public function index()
    {
        $banner = Banner::query()
            ->select([
                'id',
                'slug',
                'title',
                'position',
                'keywords',
                'short_description',
                'thumbnails',
                'link',
                'status',
            ])
            ->where('status','=', 1)
            ->get();

        return $this->item($banner);
    }

    public function getPaginate(Request $request)
    {
        $banner = Banner::query()
            ->select([
                'id',
                'slug',
                'title',
                'position',
                'keywords',
                'short_description',
                'thumbnails',
                'link',
                'status',
            ])
            ->DataTablePaginate($request);

        return $this->item($banner);
    }

    public function create(BannerCreateRequest $request)
    {
        $validatedData = $request->validated();
        $slugify = new Slugify();
        $payload = [];

        $payload['slug']                                        = $slugify->slugify($validatedData['slug']);
        $payload['title']                                       = $validatedData['title'];
        $payload['position']                                    = $validatedData['position'];
        $payload['keywords']                                    = $validatedData['keywords'];
        $payload['short_description']                           = $validatedData['short_description'];
        $payload['thumbnails']                                  = $validatedData['thumbnails'];
        $payload['link']                                        = $validatedData['link'];
        $payload['status']                                      = $validatedData['status'];

        // Kiểm tra trùng tên
        if (! $this->checkDuplicateName($payload['title'])) {
            $this->setMessage('Đã tồn tại tiêu đề');
            $this->setStatusCode(400);
            return $this->respond();
        }

        // Tạo và lưu
        $banner = Banner::query()->create($payload);
        DB::beginTransaction();

        try {
            $banner->save();
            DB::commit();
            // Trả kết quả
            $this->setMessage('Thêm bài viết thành công!');
            $this->setStatusCode(200);
            $this->setData($banner);
        } catch (Exception $e) {
            report($e);
            DB::rollBack();
            // Thông báo lỗi
            $this->setMessage($e->getMessage());
            $this->setStatusCode(500);
        }
        return $this->respond();
    }

    public function show($id)
    {
        return Banner::query()->findOrFail($id);
    }

    public function update(BannerCreateRequest $request, $id)
    {
        $validatedData = $request->validated();
        $slugify = new Slugify();
        $banner = Banner::query()->findOrFail($id);
        if (!$banner) {
            $this->setMessage('Không có bài viết này!');
            $this->setStatusCode(400);
        } else {
            DB::beginTransaction();

            if($request->changeImages == 1)
            {
                File::delete('images/Banner/'.$banner->thumbnails);
            }

            try {
                // Cập nhật
                $banner->slug                               = $slugify->slugify($validatedData['slug']);
                $banner->title                              = $validatedData['title'];
                $banner->position                           = $validatedData['position'];
                $banner->keywords                           = $validatedData['keywords'];
                $banner->short_description                  = $validatedData['short_description'];
                $banner->thumbnails                         = $validatedData['thumbnails'];
                $banner->link                               = $validatedData['link'];
                $banner->status                             = $validatedData['status'];

                $banner->save();
                DB::commit();

                // Trả về kết quả
                $this->setMessage('Cập nhật thành công');
                $this->setStatusCode(200);
                $this->setData($banner->thumbnails);
            } catch (Exception $e) {
                report($e);
                DB::rollBack();

                // Thông báo lỗi
                $this->setMessage($e->getMessage());
                $this->setStatusCode(500);
            }
        }
        return $this->respond();
    }

    public function remove($id)
    {
        $banner = Banner::query()->where('id', '=', $id);
        $banner->delete();

        return response()
            ->json(['message' => 'Success: Bạn đã xóa thành công!']);
    }

    private function checkDuplicateName($title)
    {
        $banner = Banner::query()->get();
        foreach ($banner->pluck('title') as $item) {
            if ($title == $item) {
                return false;
            }
        }
        return true;
    }

    public function searchAll(Request $request)
    {
        $search = $request->keyText;

        $banner = Banner::query()
            ->select([
                'id',
                'slug',
                'title',
                'position',
                'keywords',
                'short_description',
                'thumbnails',
                'link',
                'status',
            ])
            ->where('title', 'LIKE', "%$search%")
            ->where('status', '=', 1)
            ->DataTablePaginate($request);
        return $this->item($banner);
    }

    public function checkActive($id, $status)
    {
        $banner = Banner::query()->findOrFail($id);
        if (!$banner) {
            $this->setMessage('Không có bài viết này');
            $this->setStatusCode(400);
        } else {
            DB::beginTransaction();

            try {
                // Cập nhật trạng thái
                $banner->status = $status;

                $banner->save();
                DB::commit();

                // Trả về kết quả
                $this->setMessage('Cập nhật trạng thái thành công');
                $this->setStatusCode(200);
                $this->setData($banner);
            } catch (Exception $e) {
                report($e);
                DB::rollBack();

                // Thông báo lỗi
                $this->setMessage($e->getMessage());
                $this->setStatusCode(500);
            }
        }
        return $this->respond();
    }

    public function upload(Request $request)
    {
        if ($request->hasFile('image')) {
            try {
                $Carbon = new Carbon();
                $theTime = Carbon::now()->format('Y-m-d');
                $theImageName = $theTime . '-' . $request->image->getClientOriginalName();
                $request->image->move(public_path('images/Banner'), $theImageName);

                $this->setMessage('Thêm ảnh thành công!');
                $this->setStatusCode(200);
                $this->setData($theImageName);
                return $this->respond();
            }
            catch (Exception $e) {
                report($e);
                DB::rollBack();
                // Thông báo lỗi
                $this->setMessage($e->getMessage());
                $this->setStatusCode(500);
            }
        }
        return $this->respond();
    }
}
