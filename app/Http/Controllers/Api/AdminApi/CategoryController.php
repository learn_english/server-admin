<?php

namespace App\Http\Controllers;
use App\Http\Requests\CategoryCreateRequest;
use App\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Cocur\Slugify\Slugify;

class CategoryController extends AbstractApiController
{
    public function index()
    {
        $category = Category::query()
            ->select([
                'id',
                'slug',
                'name',
                'type_menu',
                'ordering',
                'status',
            ])
            ->where('status','=', 1)
            ->get();
        return response()->json($category, 200);
    }

    public function getPaginate(Request $request)
    {
        $category = Category::query()
            ->select([
                'id',
                'slug',
                'name',
                'type_menu',
                'ordering',
                'status',
            ])
            ->where('status','=', 1)
            ->DataTablePaginate($request);
//        ->paginate();

        return $this->item($category);
//        return $this->paginator($category, $request);
    }

    public function create(CategoryCreateRequest $request)
    {
        $validatedData = $request->validated();
        $slugify = new Slugify();
        $payload = [];

        $payload['slug'] = $slugify->slugify($validatedData['slug']);
        $payload['name'] = $validatedData['name'];
        $payload['type_menu'] = $validatedData['type_menu'];
        $payload['ordering'] = $validatedData['ordering'];
        $payload['status'] = $validatedData['status'];

        // Kiểm tra trùng tên
        if (! $this->checkDuplicateName($payload['name'])) {
            $this->setMessage('Đã tồn tại danh mục');
            $this->setStatusCode(400);
            return $this->respond();
        }

        // Tạo và lưu danh mục
        $category = Category::query()->create($payload);
        DB::beginTransaction();

        try {
            $category->save();
            DB::commit();
            // Trả kết quả
            $this->setMessage('Thêm danh mục thành công!');
            $this->setStatusCode(200);
            $this->setData($category);
        } catch (Exception $e) {
            report($e);
            DB::rollBack();
            // Thông báo lỗi
            $this->setMessage($e->getMessage());
            $this->setStatusCode(500);
        }
        return $this->respond();
    }

    public function show($id)
    {
        return Category::query()->findOrFail($id);
    }

    public function update(CategoryCreateRequest $request, $id)
    {
        $validatedData = $request->validated();
        $slugify = new Slugify();

        $category = Category::query()->findOrFail($id);
        if (!$category) {
            $this->setMessage('Không có danh mục này');
            $this->setStatusCode(400);
        } else {
            DB::beginTransaction();

            try {
                // Cập nhật tên danh mục
                $category->slug = $slugify->slugify($validatedData['slug']);
                $category->name = $validatedData['name'];
                $category->type_menu = $validatedData['type_menu'];
                $category->ordering = $validatedData['ordering'];
                $category->status = $validatedData['status'];

                $category->save();
                DB::commit();

                // Trả về kết quả
                $this->setMessage('Cập nhật thành công');
                $this->setStatusCode(200);
                $this->setData($category);
            } catch (Exception $e) {
                report($e);
                DB::rollBack();

                // Thông báo lỗi
                $this->setMessage($e->getMessage());
                $this->setStatusCode(500);
            }
        }
        return $this->respond();
    }

    public function remove($id)
    {
        $category = Category::query()->where('id', '=', $id);
        $category->delete();

        return response()
            ->json(['message' => 'Success: Bạn đã xóa thành công!']);
    }

    private function checkDuplicateName($name)
    {
        $category = Category::query()->get();
        foreach ($category->pluck('name') as $item) {
            if ($name == $item) {
                return false;
            }
        }
        return true;
    }

    public function searchAll(Request $request)
    {
        $search = $request->keyText;

        $category = Category::query()
            ->select([
                'id',
                'slug',
                'name',
                'type_menu',
                'ordering',
                'status',
            ])
            ->where('name', 'LIKE', "%$search%")
            ->where('status','=', 1)
            ->DataTablePaginate($request);
        return $this->item($category);
    }

    public function checkActive($id, $status)
    {
        $category = Category::query()->findOrFail($id);
        if (!$category) {
            $this->setMessage('Không có đơn vị này');
            $this->setStatusCode(400);
        } else {
            DB::beginTransaction();

            try {
                // Cập nhật trạng thái
                $category->status = $status;

                $category->save();
                DB::commit();

                // Trả về kết quả
                $this->setMessage('Cập nhật trạng thái thành công');
                $this->setStatusCode(200);
                $this->setData($category);
            } catch (Exception $e) {
                report($e);
                DB::rollBack();

                // Thông báo lỗi
                $this->setMessage($e->getMessage());
                $this->setStatusCode(500);
            }
        }
        return $this->respond();
    }
}
