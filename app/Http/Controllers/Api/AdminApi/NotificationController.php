<?php

namespace App\Http\Controllers;

use App\Http\Requests\NotificationCreateRequest;
use App\Notification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Cocur\Slugify\Slugify;

class NotificationController extends AbstractApiController
{
    public function index()
    {
        $notification = Notification::query()
            ->select([
                'id',
                'slug',
                'title',
                'status',
            ])
            ->where('status','=', 1)
            ->get();
        return response()->json($notification, 200);
    }

    public function getPaginate(Request $request)
    {
        $notification = Notification::query()
            ->select([
                'id',
                'slug',
                'title',
                'status',
            ])
            ->where('status','=', 1)
            ->DataTablePaginate($request);

        return $this->item($notification);
    }

    public function create(NotificationCreateRequest $request)
    {
        $validatedData = $request->validated();
        $slugify = new Slugify();
        $payload = [];

        $payload['slug'] = $slugify->slugify($validatedData['slug']);
        $payload['title'] = $validatedData['title'];
        $payload['status'] = $validatedData['status'];

        // Kiểm tra trùng tên
        if (! $this->checkDuplicateName($payload['title'])) {
            $this->setMessage('Đã tồn tại thông báo');
            $this->setStatusCode(400);
            return $this->respond();
        }

        // Tạo và lưu danh mục
        $notification = Notification::query()->create($payload);
        DB::beginTransaction();

        try {
            $notification->save();
            DB::commit();
            // Trả kết quả
            $this->setMessage('Thêm thông báo thành công!');
            $this->setStatusCode(200);
            $this->setData($notification);
        } catch (Exception $e) {
            report($e);
            DB::rollBack();
            // Thông báo lỗi
            $this->setMessage($e->getMessage());
            $this->setStatusCode(500);
        }
        return $this->respond();
    }

    public function show($id)
    {
        return Notification::query()->findOrFail($id);
    }

    public function update(NotificationCreateRequest $request, $id)
    {
        $validatedData = $request->validated();
        $slugify = new Slugify();
        $notification = Notification::query()->findOrFail($id);
        if (!$notification) {
            $this->setMessage('Không có thông báo này');
            $this->setStatusCode(400);
        } else {
            DB::beginTransaction();

            try {
                // Cập nhật tên danh mục
                $notification->slug = $slugify->slugify($validatedData['slug']);
                $notification->title = $validatedData['title'];
                $notification->status = $validatedData['status'];

                $notification->save();
                DB::commit();

                // Trả về kết quả
                $this->setMessage('Cập nhật thành công');
                $this->setStatusCode(200);
                $this->setData($notification);
            } catch (Exception $e) {
                report($e);
                DB::rollBack();

                // Thông báo lỗi
                $this->setMessage($e->getMessage());
                $this->setStatusCode(500);
            }
        }
        return $this->respond();
    }

    public function remove($id)
    {
        $notification = Notification::query()->where('id', '=', $id);
        $notification->delete();

        return response()
            ->json(['message' => 'Success: Bạn đã xóa thành công!']);
    }

    private function checkDuplicateName($title)
    {
        $notification = Notification::query()->get();
        foreach ($notification->pluck('title') as $item) {
            if ($title == $item) {
                return false;
            }
        }
        return true;
    }

    public function searchAll(Request $request)
    {
        $search = $request->keyText;

        $notification = Notification::query()
            ->select([
                'id',
                'slug',
                'title',
                'status',
            ])
            ->where('status','=', 1)
            ->where('title', 'LIKE', "%$search%")
            ->DataTablePaginate($request);
        return $this->item($notification);
    }

    public function checkActive($id, $status)
    {
        $notification = Notification::query()->findOrFail($id);
        if (!$notification) {
            $this->setMessage('Không có thông báo này');
            $this->setStatusCode(400);
        } else {
            DB::beginTransaction();

            try {
                // Cập nhật trạng thái
                $notification->status = $status;

                $notification->save();
                DB::commit();

                // Trả về kết quả
                $this->setMessage('Cập nhật trạng thái thành công');
                $this->setStatusCode(200);
                $this->setData($notification);
            } catch (Exception $e) {
                report($e);
                DB::rollBack();

                // Thông báo lỗi
                $this->setMessage($e->getMessage());
                $this->setStatusCode(500);
            }
        }
        return $this->respond();
    }
}
