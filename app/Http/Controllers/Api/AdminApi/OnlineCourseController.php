<?php

namespace App\Http\Controllers;

use App\Http\Requests\OnlineCourseCreateRequest;
use App\OnlineCourse;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Cocur\Slugify\Slugify;

class OnlineCourseController extends AbstractApiController
{
    public function index()
    {
        $onlineCourse = OnlineCourse::query()
            ->select([
                'id',
                'slug',
                'title',
                'price',
                'keywords',
                'short_description',
                'link',
                'type_online',
                'pending_delete',
                'status',
            ])
            ->where('status','=', 1)
            ->get();

        return $this->item($onlineCourse);
    }

    public function getPaginate(Request $request)
    {
        $onlineCourse = OnlineCourse::query()
            ->select([
                'id',
                'slug',
                'title',
                'price',
                'keywords',
                'short_description',
                'link',
                'type_online',
                'pending_delete',
                'status',
            ])
            ->DataTablePaginate($request);

        return $this->item($onlineCourse);
    }

    public function create(OnlineCourseCreateRequest $request)
    {
        $validatedData = $request->validated();
        $slugify = new Slugify();
        $payload = [];

        $payload['slug']                                        = $slugify->slugify($validatedData['slug']);
        $payload['title']                                       = $validatedData['title'];
        $payload['price']                                       = $validatedData['price'];
        $payload['keywords']                                    = $validatedData['keywords'];
        $payload['short_description']                           = $validatedData['short_description'];
        $payload['link']                                        = $validatedData['link'];
        $payload['type_online']                                 = $validatedData['type_online'];
        $payload['status']                                      = $validatedData['status'];

        // Kiểm tra trùng tên
        if (! $this->checkDuplicateName($payload['title'])) {
            $this->setMessage('Đã tồn tại tiêu đề');
            $this->setStatusCode(400);
            return $this->respond();
        }

        // Tạo và lưu
        $onlineCourse = OnlineCourse::query()->create($payload);
        DB::beginTransaction();

        try {
            $onlineCourse->save();
            DB::commit();
            // Trả kết quả
            $this->setMessage('Thêm bài viết thành công!');
            $this->setStatusCode(200);
            $this->setData($onlineCourse);
        } catch (Exception $e) {
            report($e);
            DB::rollBack();
            // Thông báo lỗi
            $this->setMessage($e->getMessage());
            $this->setStatusCode(500);
        }
        return $this->respond();
    }

    public function show($id)
    {
        return OnlineCourse::query()->findOrFail($id);
    }

    public function update(OnlineCourseCreateRequest $request, $id)
    {
        $validatedData = $request->validated();
        $slugify = new Slugify();
        $onlineCourse = OnlineCourse::query()->findOrFail($id);
        if (!$onlineCourse) {
            $this->setMessage('Không có bài viết này!');
            $this->setStatusCode(400);
        } else {
            DB::beginTransaction();

            try {
                // Cập nhật
                $onlineCourse->slug                               = $slugify->slugify($validatedData['slug']);
                $onlineCourse->title                              = $validatedData['title'];
                $onlineCourse->price                              = $validatedData['price'];
                $onlineCourse->keywords                           = $validatedData['keywords'];
                $onlineCourse->short_description                  = $validatedData['short_description'];
                $onlineCourse->link                               = $validatedData['link'];
                $onlineCourse->type_online                        = $validatedData['type_online'];
                $onlineCourse->status                             = $validatedData['status'];

                $onlineCourse->save();
                DB::commit();

                // Trả về kết quả
                $this->setMessage('Cập nhật thành công');
                $this->setStatusCode(200);
                $this->setData($onlineCourse);
            } catch (Exception $e) {
                report($e);
                DB::rollBack();

                // Thông báo lỗi
                $this->setMessage($e->getMessage());
                $this->setStatusCode(500);
            }
        }
        return $this->respond();
    }

    public function remove($id)
    {
        $onlineCourse = OnlineCourse::query()->where('id', '=', $id);
        $onlineCourse->delete();

        return response()
            ->json(['message' => 'Success: Bạn đã xóa thành công!']);
    }

    private function checkDuplicateName($title)
    {
        $onlineCourse = OnlineCourse::query()->get();
        foreach ($onlineCourse->pluck('title') as $item) {
            if ($title == $item) {
                return false;
            }
        }
        return true;
    }

    public function searchAll(Request $request)
    {
        $search = $request->keyText;

        $onlineCourse = OnlineCourse::query()
            ->select([
                'id',
                'slug',
                'title',
                'price',
                'keywords',
                'short_description',
                'link',
                'type_online',
                'pending_delete',
                'status',
            ])
            ->where('title', 'LIKE', "%$search%")
            ->where('status', '=', 1)
            ->DataTablePaginate($request);
        return $this->item($onlineCourse);
    }

    public function checkActive($id, $status)
    {
        $onlineCourse = OnlineCourse::query()->findOrFail($id);
        if (!$onlineCourse) {
            $this->setMessage('Không có bài viết này');
            $this->setStatusCode(400);
        } else {
            DB::beginTransaction();

            try {
                // Cập nhật trạng thái
                $onlineCourse->status = $status;

                $onlineCourse->save();
                DB::commit();

                // Trả về kết quả
                $this->setMessage('Cập nhật trạng thái thành công');
                $this->setStatusCode(200);
                $this->setData($onlineCourse);
            } catch (Exception $e) {
                report($e);
                DB::rollBack();

                // Thông báo lỗi
                $this->setMessage($e->getMessage());
                $this->setStatusCode(500);
            }
        }
        return $this->respond();
    }

    public function upload(Request $request)
    {
        if ($request->hasFile('image')) {
            try {
                $Carbon = new Carbon();
                $theTime = Carbon::now()->format('Y-m-d');
                $theImageName = $theTime . '-' . $request->image->getClientOriginalName();
                $request->image->move(public_path('images/OnlineCourse'), $theImageName);

                $this->setMessage('Thêm ảnh thành công!');
                $this->setStatusCode(200);
                $this->setData($theImageName);
                return $this->respond();
            }
            catch (Exception $e) {
                report($e);
                DB::rollBack();
                // Thông báo lỗi
                $this->setMessage($e->getMessage());
                $this->setStatusCode(500);
            }
        }
        return $this->respond();
    }
}
