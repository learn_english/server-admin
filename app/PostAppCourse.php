<?php

namespace App;

use App\Support\DataTablePaginate;
use Illuminate\Database\Eloquent\Model;

class PostAppCourse extends Model
{
    use DataTablePaginate;

    protected $fillable = [
        'post_id',
        'app_course_id',
    ];

    protected $filter = [
        'id',
        'post_id',
        'app_course_id',
    ];
}
