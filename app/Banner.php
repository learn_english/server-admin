<?php

namespace App;

use App\Support\DataTablePaginate;
use Illuminate\Database\Eloquent\Model;

class Banner extends Model
{
    use DataTablePaginate;

    protected $fillable = [
        'slug',
        'title',
        'position',
        'keywords',
        'short_description',
        'thumbnails',
        'link',
        'status',
    ];

    protected $filter = [
        'id',
        'slug',
        'title',
        'position',
        'keywords',
        'short_description',
        'thumbnails',
        'link',
        'status',
    ];
}
