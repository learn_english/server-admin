<?php

namespace App;

use App\Support\DataTablePaginate;
use Illuminate\Database\Eloquent\Model;

class TeachingCenterLiveCourse extends Model
{
    use DataTablePaginate;

    protected $fillable = [
        'teaching_center_id',
        'live_course_id',
    ];

    protected $filter = [
        'id',
        'teaching_center_id',
        'live_course_id',
    ];
}
