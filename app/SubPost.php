<?php

namespace App;

use App\Support\DataTablePaginate;
use Illuminate\Database\Eloquent\Model;

class SubPost extends Model
{
    use DataTablePaginate;

    protected $fillable = [
        'slug',
        'title',
        'keywords',
        'short_description',
        'description',
        'thumbnails',
//        'author',
//        'approved_by',
//        'approved_at',
//        'rejected_by',
//        'rejected_at',
        'pending_delete',
        'status',
        'category_id',
        'post_id',
    ];

    protected $filter = [
        'id',
        'slug',
        'title',
        'keywords',
        'short_description',
        'description',
        'thumbnails',
//        'author',
//        'approved_by',
//        'approved_at',
//        'rejected_by',
//        'rejected_at',
        'pending_delete',
        'status',
        'category_id',
        'post_id',
    ];
}
