<?php

namespace App;

use App\Support\DataTablePaginate;
use Illuminate\Database\Eloquent\Model;

class OnlineCourse extends Model
{
    use DataTablePaginate;

    protected $table = "online_courses";

    protected $fillable = [
        'slug',
        'title',
        'price',
        'keywords',
        'short_description',
        'link',
        'type_online',
        'pending_delete',
        'status',
    ];

    protected $filter = [
        'id',

        // BasicInfo
        'slug',
        'title',
        'price',
        'type_app',
        // Description
        'keywords',
        'short_description',
        'link',

        // MediaInfo
        'type_online',

        // OrderInfo
        'pending_delete',
        'status',
    ];
}
