<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTeachingCenterLiveCoursesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('teaching_center_live_courses', function (Blueprint $table) {
            $table->id();

            $table->unsignedBigInteger('teaching_center_id')->comment('Mã trung tâm, khóa ngoại');
            $table->unsignedBigInteger('live_course_id')->comment('Mã khóa học trực tiếp, khóa ngoại');

            $table->foreign('teaching_center_id')
                ->references('id')->on('teaching_centers')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->foreign('live_course_id')
                ->references('id')->on('live_courses')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('teaching_center_live_courses');
    }
}
