<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->id();

            // Thông tin cơ bản
            $table->string('slug', 200)->comment('Đường dẫn thân thiện, SEO URL');
            $table->string('title', 200)->comment('Tiêu đề bài viết');

            $table->string('keywords', 200)->comment('Từ khóa');
            $table->string('short_description', 200)->comment('Mô tả ngắn');

            // Mô tả
            $table->text('description')->comment('Mô tả thêm');

            // Hình ảnh
            $table->string('thumbnails')->default('')->comment('Hình đại diện');

            // Thông tin người đăng, người duyệt
//            $table->unsignedBigInteger('author')->default(0)->comment('Người đăng');
//            $table->unsignedBigInteger('approved_by')->nullable()->comment('Người phê duyệt');
//            $table->timestamp('approved_at')->nullable()->comment('Thời gian duyệt');
//            $table->unsignedBigInteger('rejected_by')->nullable()->comment('Người từ chối');
//            $table->timestamp('rejected_at')->nullable()->comment('Thời từ chối');

            // Thông tin bổ sung, bổ trợ tìm kiếm
            $table->unsignedBigInteger('category_id')->nullable()->comment('Mã danh mục');

            $table->timestamp('pending_delete')->nullable()->comment('Thời gian xóa vĩnh viễn');
            $table->unsignedTinyInteger('status')->default(1)->comment('Trang thái bài đăng');

            $table->foreign('category_id')
                ->references('id')->on('categories')
                ->onUpdate('restrict')
                ->onDelete('restrict');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
    }
}
