<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePostAppCoursesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('post_app_courses', function (Blueprint $table) {
            $table->id();

            $table->unsignedBigInteger('post_id')->comment('Mã bài đăng, khóa ngoại');
            $table->unsignedBigInteger('app_course_id')->comment('Mã khóa học online, khóa ngoại');

            $table->foreign('post_id')
                ->references('id')->on('posts')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->foreign('app_course_id')
                ->references('id')->on('app_courses')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('post_app_courses');
    }
}
