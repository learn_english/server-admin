<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePostLiveCoursesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('post_live_courses', function (Blueprint $table) {
            $table->id();

            $table->unsignedBigInteger('post_id')->comment('Mã bài đăng, khóa ngoại');
            $table->unsignedBigInteger('live_course_id')->comment('Mã khóa học trực tiếp, khóa ngoại');

            $table->foreign('post_id')
                ->references('id')->on('posts')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->foreign('live_course_id')
                ->references('id')->on('live_courses')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('post_live_courses');
    }
}
