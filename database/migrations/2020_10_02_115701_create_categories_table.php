<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categories', function (Blueprint $table) {
            $table->id();

            $table->string('slug', 200)->comment('Đường dẫn thân thiện, SEO URL');
            $table->string('name')->comment('Tên danh mục');

            $table->unsignedInteger('ordering')->default(1)->comment('Sắp xếp');
            $table->unsignedInteger('type_menu')->default(1)->comment('Loại menu');
            $table->unsignedTinyInteger('status')->default(1)->comment('Trang thái');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('categories');
    }
}
