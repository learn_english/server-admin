<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePostOnlineCoursesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('post_online_courses', function (Blueprint $table) {
            $table->id();

            $table->unsignedBigInteger('post_id')->comment('Mã bài đăng, khóa ngoại');
            $table->unsignedBigInteger('online_course_id')->comment('Mã khóa học online, khóa ngoại');

            $table->foreign('post_id')
                ->references('id')->on('posts')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->foreign('online_course_id')
                ->references('id')->on('online_courses')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('post_online_courses');
    }
}
