<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOnlineCoursesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('online_courses', function (Blueprint $table) {
            $table->id();

            // Thông tin cơ bản
            $table->string('slug', 200)->comment('Đường dẫn thân thiện, SEO URL');
            $table->string('title', 200)->comment('Tiêu đề bài viết');
            // Học phí
            $table->double('price')->default(0)->comment('Học phí');
            $table->string('keywords', 200)->comment('Từ khóa');
            $table->string('short_description', 200)->comment('Mô tả ngắn');

            $table->unsignedInteger('type_online')->default(1)->comment('Loại hình 1: online, 2: web, 3: mobile');

            // Hình ảnh, video, link
            $table->string('link')->default('')->comment('Link dẫn web');

            // Thông tin người đăng, người duyệt
//            $table->unsignedBigInteger('author')->default(0)->comment('Người đăng');
//            $table->unsignedBigInteger('approved_by')->nullable()->comment('Người phê duyệt');
//            $table->timestamp('approved_at')->nullable()->comment('Thời gian duyệt');
//            $table->unsignedBigInteger('rejected_by')->nullable()->comment('Người từ chối');
//            $table->timestamp('rejected_at')->nullable()->comment('Thời gian từ chối');

            // Thông tin bổ sung, bổ trợ tìm kiếm
            $table->timestamp('pending_delete')->nullable()->comment('Thời gian xóa vĩnh viễn');
            $table->unsignedTinyInteger('status')->default(1)->comment('Trang thái bài đăng');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('online_courses');
    }
}
