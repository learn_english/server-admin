<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRegisterCoursesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('register_courses', function (Blueprint $table) {
            $table->id();

            // Thông tin
            $table->string('last_name')->default('')->comment('Họ');
            $table->string('mid_name')->default('')->comment('Tên đệm');
            $table->string('first_name')->default('')->comment('Tên');

            $table->string('email')->default('')->comment('Địa chỉ email');
            $table->string('mobile')->default('')->comment('Số điện thoại liên hệ');
            $table->string('address')->default('')->comment('Địa chỉ liên hệ');

            // Thông tin cá nhân
            $table->tinyInteger('sex')->default(1)->comment('Giới tính 0 = nữ, 1 = nam');

            $table->unsignedBigInteger('teaching_center_id')->comment('Mã trung tâm, khóa ngoại');
            $table->unsignedBigInteger('live_course_id')->comment('Mã khóa học trực tiếp, khóa ngoại');

            $table->string('last_name_child')->default('')->comment('Họ trẻ');
            $table->string('mid_name_child')->default('')->comment('Tên đệm trẻ');
            $table->string('first_name_child')->default('')->comment('Tên trẻ');
            $table->date('birthday_child')->comment('Ngày sinh trẻ');

            $table->unsignedInteger('why_know')->comment('Biết đến khóa học từ: 0: Facebook, 1: Google, 2: Tạp chí, 3: Người thân, bạn bè, 4: Sự kiện, 5: radio, 6: Quảng cảo ngoài trời, 7: tờ rơi');

            $table->foreign('teaching_center_id')
                ->references('id')->on('teaching_centers')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->foreign('live_course_id')
                ->references('id')->on('live_courses')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('register_courses');
    }
}
