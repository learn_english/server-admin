<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {

            $table->id();
            // Thông tin đăng nhập
            $table->string('username')->unique()->comment('Tên tài khoản');
            $table->string('password')->comment('Mật khẩu, đã băm bằng bcrypt');
            $table->string('email')->unique()->comment('Địa chỉ email');
            $table->string('mobile')->unique()->comment('Số điện thoại liên hệ');
            $table->string('address')->nullable()->comment('Địa chỉ liên hệ');
            $table->timestamp('email_verified_at')->nullable()->comment('Email xác minh thành công khi nào');

            // Thông tin cá nhân
            $table->string('identity_number')->default('')->comment('CMND/Thẻ căn cước');
            $table->string('last_name')->comment('Họ');
            $table->string('first_name')->comment('Tên');
            $table->tinyInteger('sex')->default(1)->comment('Giới tính 0 = nữ, 1 = nam');

            // Quyền hạn
            $table->string('role')->default('author');
            $table->boolean('status')->default(true)->comment('Tình trạng khóa tài khoản');

            $table->rememberToken();
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
