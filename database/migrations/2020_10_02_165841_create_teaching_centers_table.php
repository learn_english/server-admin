<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTeachingCentersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('teaching_centers', function (Blueprint $table) {
            $table->id();

            // Thông tin cơ bản
            $table->string('slug', 200)->comment('Đường dẫn thân thiện, SEO URL');
            $table->string('name', 200)->comment('Tiêu trung tâm');

            $table->string('keywords', 200)->comment('Từ khóa');
            $table->string('short_description', 200)->comment('Mô tả ngắn');

            // Thông tin chung - ckeditor
            $table->text('info')->comment('Thông tin khóa học');
            $table->text('time_table')->comment('Lịch học');
            $table->text('contact_table')->comment('Liên hệ và địa điểm');
            $table->text('equipment')->comment('Trang thiết bị');

            // Bản đồ
            $table->decimal('longitude', 10, 7)->nullable()->comment('Kinh độ');
            $table->decimal('latitude', 10, 7)->nullable()->comment('Vĩ độ');

            // Hình ảnh, video và vị trí
            $table->string('thumbnails')->default('')->comment('Hình đại diện');

            // Liên hệ
            $table->string('contact_mobile')->default('')->comment('SĐT di động liên hệ');
            $table->string('contact_phone')->default('')->comment('SĐT cố định liên hệ');
            $table->string('contact_email')->default('')->comment('Email liên hệ');
            $table->string('contact_address')->default('')->comment('Địa chỉ liên hệ');

            $table->timestamp('pending_delete')->nullable()->comment('Thời gian xóa vĩnh viễn');
            $table->unsignedTinyInteger('status')->default(1)->comment('Trang thái');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('teaching_centers');
    }
}
