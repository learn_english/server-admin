<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNewslettersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('newsletters', function (Blueprint $table) {
            $table->id();

            // Thông tin
            $table->string('full_name')->unique()->comment('Họ tên');
            $table->string('email')->unique()->comment('Địa chỉ email');
            $table->string('mobile')->unique()->comment('Số điện thoại liên hệ');
            $table->string('address')->nullable()->comment('Địa chỉ liên hệ');

            $table->tinyInteger('sex')->default(1)->comment('Giới tính 0 = nữ, 1 = nam');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('newsletters');
    }
}
