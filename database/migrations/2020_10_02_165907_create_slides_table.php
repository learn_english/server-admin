<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSlidesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('slides', function (Blueprint $table) {
            $table->id();

            $table->string('slug', 200)->comment('Đường dẫn thân thiện, SEO URL');
            $table->string('title')->comment('Tên slide');
            $table->string('thumbnails')->default('')->comment('Ảnh slide');

            $table->string('short_description')->default('')->comment('Nội dung slide');
            $table->string('link')->default('')->comment('Đường dẫn slide');

            $table->unsignedInteger('ordering')->default(1)->comment('Sắp xếp');
            $table->unsignedTinyInteger('status')->default(1)->comment('Trang thái');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('slides');
    }
}
