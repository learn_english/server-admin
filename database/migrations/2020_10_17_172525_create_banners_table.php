<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBannersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('banners', function (Blueprint $table) {
            $table->id();

            // Thông tin cơ bản
            $table->string('slug', 200)->comment('Đường dẫn thân thiện, SEO URL');
            $table->string('title', 200)->comment('Tiêu đề bài viết');

            // Hình ảnh
            $table->string('thumbnails')->default('')->comment('Hình đại diện');
            $table->string('keywords', 200)->comment('Từ khóa');
            $table->string('short_description', 200)->comment('Mô tả ngắn');

            $table->tinyInteger('position')->default(1)->comment('Vị trí: 0 = trái, 1 = phải');
            $table->string('link')->default('')->comment('Link dẫn web banner');

            $table->unsignedTinyInteger('status')->default(1)->comment('Trang thái bài đăng');


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('banners');
    }
}
