<?php

use App\User;
use Illuminate\Database\Seeder;

class CreateSampleUser extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::query()->create(
            [
                'username' => 'demoad',
                'password' => bcrypt('123456'),
                'email' => 'tanthanhphuochoa@gmail.com',
                'mobile' => '0937519106',
                'last_name' => 'Nguyễn',
                'first_name' => 'Tấn Thành',
                'identity_number' => '123456789',
                'sex' => 1,
                'address' => "Bình Dương",
                'role' => 'Administrator',
                'status' => 1,
            ]
        );
    }
}
